import { MASTER_COURT_SEARCH, MASTER_COURT_GET_BY_ID } from "../actions/MasterCourt";

function masterCourtListReducer(state = [], action) {
    switch (action.type) {
        case MASTER_COURT_SEARCH: return action.payload.data;
        default: return state;
    }
}

function masterCourtDetailReducer(state = {}, action) {
    switch(action.type) {
        case MASTER_COURT_GET_BY_ID: return action.payload.data;
        default: return state;
    }
}

export default {
    masterCourtListReducer,
    masterCourtDetailReducer
}