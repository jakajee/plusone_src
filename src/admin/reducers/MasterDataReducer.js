import { GET_MASTER_FACILITY_LIST, GET_MASTER_SPORT_LIST } from "../actions/MasterData";
import { getDefaultPleaseSelect } from "../util/util";

export function masterFacilityReducer(state = [], action) {
    switch (action.type) {
        case GET_MASTER_FACILITY_LIST: {
            return [...action.payload.data].map(item => ({
                text: item.facilityName,
                value: item.uuid
            }))
        }

        default: return state;
    }
}

export function masterSportReducer(state = [], action) {
    switch (action.type) {
        case GET_MASTER_SPORT_LIST: {
            return [getDefaultPleaseSelect("sportName", "uuid"), ...action.payload.data].map(item => ({
                text: item.sportName,
                value: item.uuid
            }))
        }

        default: return state;
    }
}