import { GET_SYSTEM_AREA_LIST, GET_SYSTEM_PROVINCE_LIST } from '../actions/SystemData';
import { getDefaultPleaseSelect } from '../util/util';


export function systemProvinceReducer(state = [], action) {
    switch (action.type) {
        case GET_SYSTEM_PROVINCE_LIST: {
            return [getDefaultPleaseSelect("provinceName", "provinceId"), ...action.payload.data].map(item => ({
                text: item.provinceName,
                value: item.provinceId
            }));;
        }

        default: return state;
    }
}

export function systemAreaReducer(state = [], action) {
    switch (action.type) {
        case GET_SYSTEM_AREA_LIST: {
            return [getDefaultPleaseSelect("areaName", "areaId"), ...action.payload.data].map(item => ({
                text: item.areaName,
                value: item.areaId
            }))
        }

        default: return state;
    }
}

export default {
    systemAreaReducer,
    systemProvinceReducer
}