import { combineReducers } from "redux";
import masterCourtReducer from "./masterCourtReducer";
import {
    systemAreaReducer,
    systemProvinceReducer
} from './SystemDataReducer';
import { masterSportReducer, masterFacilityReducer } from "./MasterDataReducer";
import { masterEventListReducer } from "./MasterEventReducer";

const rootReducer = combineReducers({    
    auth: (state = '') => state,
    masterCourtList: masterCourtReducer.masterCourtListReducer,
    systemProvinceList: systemProvinceReducer,
    systemAreaList: systemAreaReducer,
    masterSportList: masterSportReducer,
    masterFacilityList: masterFacilityReducer,
    courtDetail: masterCourtReducer.masterCourtDetailReducer,
    masterEventList: masterEventListReducer
})

export default rootReducer;