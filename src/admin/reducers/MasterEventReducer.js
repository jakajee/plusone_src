import { MASTER_EVENT_SEARCH, MASTER_EVENT_CREATE, MASTER_EVENT_EDIT } from "../actions/MasterEvent";
import _ from 'lodash';

export const masterEventListReducer = (state = [], action) => {
    switch (action.type) {
        case MASTER_EVENT_SEARCH: return [...action.payload];
        case MASTER_EVENT_CREATE: return [action.payload, ...state];
        case MASTER_EVENT_EDIT: return editEventReducer(state, action);
        default: return state;
    }
}

const editEventReducer = (state = [], action) => {
    let currentEventList = [...state];
    const editedIndex = _.findIndex(state, { uuid: action.payload.uuid });

    currentEventList.splice(editedIndex, 1, action.payload);
    return currentEventList;
}