import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

import asyncMd from "../app/middlewares/async";
import asyncNested from '../app/middlewares/asyncNested';
import interceptors from "../app/infrastructure/interceptors";
import { getToken } from "../app/actions/User";
import rootReducer from "./reducers";

import route from "./app.route";
import { Router } from "react-router";
import appHistory from "./app.history";

interceptors();

const store = createStore(
    rootReducer,
    { auth: getToken() },
    applyMiddleware(asyncMd, asyncNested)
)

const provider =
    <Provider store={store}>
        <Router history={appHistory}>
            {route}
        </Router>
    </Provider>

ReactDOM.render(
    provider,
    document.getElementById("admin")
);

