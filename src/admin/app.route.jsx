import React from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';

import UserHeader from '../app/headers/UserHeader';
import CourtListMain from './components/courts/CourtListMain';
import PlaceList from './components/places/PlaceList';
import Home from './components/Home';
import App from '../app/components/App';
import CourtAdd from './components/courts/CourtAdd';
import CourtEdit from './components/courts/CourtEdit';
import EventListMain from './components/events/EventListMain';
import EventEdit from './components/events/EventEdit';
import EventAdd from './components/events/EventAdd';
import NotFound from '../app/components/util/NotFound';
import { getCurrentUserRoleId } from '../app/actions/User';

const renderLink = (path, title) => {
    return (
        <li className="nav-item">
            <NavLink exact={path === '/admin'} activeStyle={{backgroundColor: "#333333"}}  to={path} className="text-white nav-link">{title}</NavLink>
        </li>
    )
}

const getLayout = () => {
    if (getCurrentUserRoleId() !== 1)
        return (
            <div className="card border-warning text-center">
                <div className="card-header bg-warning">
                    <h4 className="card-title text-white">
                        พบข้อผิดพลาด !!
                    </h4>
                </div>
                <div className="card-body">
                    <NotFound alertMessage="คุณไม่มีสิทธิ์เข้าถึงการใช้งานในส่วนนี้ โปรดลองใหม่อีกครั้ง" />
                    <div>
                        <a href="/app">กลับสู่หน้าหลัก</a>
                    </div>
                </div>
            </div>
        );

    return (
        <div className="card border-success">
            <div className="card-header bg-success">
                <ul className="nav">
                    {renderLink("/admin", "หน้าหลัก")}
                    {renderLink("/admin/courts", "จัดการสนาม")}
                    {/* {renderLink("/admin/places", "จัดการสถานที่")} */}
                    {renderLink("/admin/events", "จัดการกิจกรรม")}
                </ul>
            </div>

            <div className="card-body">
                <Switch>
                    <Route exact path="/admin/courts/add" component={CourtAdd} />
                    <Route exact path="/admin/courts/:id" component={CourtEdit} />
                    <Route exact path="/admin/courts" component={CourtListMain} />

                    <Route exact path="/admin/events/add" component={EventAdd} />
                    <Route exact path="/admin/events/:id" component={EventEdit} />
                    <Route exact path="/admin/events" component={EventListMain} />

                    <Route exact path="/admin/places" component={PlaceList} />
                    <Route exact path="/admin" component={Home} />
                    <Route exact path="/" component={App} />
                    <Route component={NotFound} />
                </Switch>
            </div>
        </div>
    )
}

export default (
    <React.Fragment>
        <UserHeader />

        <div className="container mt-3">
            {getLayout()}
        </div>
    </React.Fragment>
)