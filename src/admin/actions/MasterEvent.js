import _ from 'lodash'
import { pluseoneAdmin } from '../../api/plusoneApi';
import appHistory from '../app.history';
import { getCurrentUser } from '../../app/actions/User';
import { getSportListAsync } from './MasterData';

export const MASTER_EVENT_SEARCH = "MASTER_EVENT_SEARCH";
export const MASTER_EVENT_CREATE = "MASTER_EVENT_CREATE";
export const MASTER_EVENT_EDIT = "MASTER_EVENT_EDIT";

export const getMasterEventList = params => async (dispatch) => {
    const response = await pluseoneAdmin.get(`eventmanager?eventName=${params.eventName}`);
    dispatch({
        type: MASTER_EVENT_SEARCH,
        payload: response.data
    });
}

export const getMasterEventByUuid = async eventUuid => {
    const { data } = await pluseoneAdmin.get(`eventmanager/${eventUuid}`);
    const model = _.omit(data, ['tblMasterCourt', 'eventImg']);
    model.courtName = data.tblMasterCourt.courtName;
    model.eventImg = data.eventImg.imgUrl;

    return model;
}

export const createEvent = model => async dispatch => {
    const reqModel = {
        ...model,
        eventImg: { imgUrl: model.eventImg },
        userCreated: getCurrentUser()
    }

    const sportList = await getSportListAsync();
    const sportName = _.mapKeys(sportList, 'uuid')[model.masterSportTypeUuid].sportName;

    const response = await pluseoneAdmin.post(`eventmanager`, reqModel);
    const { data } = response.data;

    dispatch({
        type: MASTER_EVENT_CREATE,
        payload: {
            uuid: data.uuid,
            eventName: data.eventName,
            eventDateTimeStart: data.eventDateTimeStart,
            eventDateTimeEnd: data.eventDateTimeEnd,
            tblMasterCourt: {
                courtName: model.courtName
            },
            tblMasterSportType: {
                sportName
            },
            dateTimeCreated: data.dateTimeCreated,
            flagChange: true
        }
    })

    appHistory.push('/admin/events');
}

export const updateEvent = model => async dispatch => {
    const reqModel = {
        ...model,
        eventImg: { imgUrl: model.eventImg },
        userModified: getCurrentUser()
    };

    const sportList = await getSportListAsync();
    const sportName = _.mapKeys(sportList, 'uuid')[model.masterSportTypeUuid].sportName;

    const { data } = await pluseoneAdmin.patch(`eventmanager/${model.uuid}`, reqModel);

    dispatch({
        type: MASTER_EVENT_EDIT,
        payload: {
            uuid: reqModel.uuid,
            eventName: reqModel.eventName,
            eventDateTimeStart: reqModel.eventDateTimeStart,
            eventDateTimeEnd: reqModel.eventDateTimeEnd,
            tblMasterCourt: {
                courtName: reqModel.courtName
            },
            tblMasterSportType: {
                sportName
            },
            dateTimeCreated: reqModel.dateTimeCreated,
            dateTimeModified: data.data.dateTimeModified,
            flagChange: true
        }
    });

    appHistory.push('/admin/events');
}