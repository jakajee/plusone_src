import axios from "axios";
import { API_PATH_ADMIN } from '../../config';

export const GET_SYSTEM_PROVINCE_LIST = 'GET_SYSTEM_PROVINCE_LIST';
export const GET_SYSTEM_AREA_LIST = 'GET_SYSTEM_AREA_LIST';

export function getProvinceList() {
    const req = axios.get(`${API_PATH_ADMIN}/system/province`)

    return {
        type: GET_SYSTEM_PROVINCE_LIST,
        payload: req
    };
}

export function getAreaList(systemProvinceId) {
    const req = axios.get(`${API_PATH_ADMIN}/system/area?systemProvinceId=${systemProvinceId || ''}`);

    return {
        type: GET_SYSTEM_AREA_LIST,
        payload: req
    }
}