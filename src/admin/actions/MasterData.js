import axios from "axios";
import { API_PATH_ADMIN } from '../../config';

let sportTypeList = [];

export const GET_MASTER_SPORT_LIST = "GET_MASTER_SPORT_LIST";
export const GET_MASTER_FACILITY_LIST = "GET_MASTER_FACILITY_LIST";

export function getSportList() {
    const req = axios.get(`${API_PATH_ADMIN}/master/sport`);

    return {
        type: GET_MASTER_SPORT_LIST,
        payload: req
    }
}

export const getSportListAsync = async () => {
    if(sportTypeList.length > 0)
        return [...sportTypeList];

    const response = await axios.get(`${API_PATH_ADMIN}/master/sport`);
    sportTypeList = response.data;
    return [...sportTypeList];
   
}

export function getFacilityList() {
    const req = axios.get(`${API_PATH_ADMIN}/master/facility`);

    return {
        type: GET_MASTER_FACILITY_LIST,
        payload: req
    }
}


export const getCourtListAsync = async params => {
    const response = await axios.get(`${API_PATH_ADMIN}/master/courts`, {
        params: {...params}
    });

    return response.data;
}