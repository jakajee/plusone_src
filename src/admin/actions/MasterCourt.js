import axios from "axios";
import { API_PATH_ADMIN } from '../../config';

export const MASTER_COURT_SEARCH = 'MASTER_COURT_SEARCH';
export const MASTER_COURT_GET_BY_ID = 'MASTER_COURT_GET_BY_ID';

export function getMasterCourtList(parameter) {
    const req = axios.get(`${API_PATH_ADMIN}/courtmanager`, {
        params: parameter
    })

    return {
        type: MASTER_COURT_SEARCH,
        payload: req,
        params: parameter
    }
}

export function createCourt(model) {
    const value = { ...model };

    value.facilityList = getSelectedFacility(model.facilityList);

    const req = axios.post(`${API_PATH_ADMIN}/courtmanager`, value);
    return req;
}

export function getCourtDetail(courtId) {
    const req = axios.get(`${API_PATH_ADMIN}/courtmanager/${courtId}`);

    return {
        type: MASTER_COURT_GET_BY_ID,
        payload: req
    }
}

export function updateCourt(model) {
    const value = { ...model };

    value.facilityList = getSelectedFacility(model.facilityList);

    return axios.patch(`${API_PATH_ADMIN}/courtmanager/${value.uuid}`, value);
}

function getSelectedFacility(facilityList) {
    return facilityList
        .filter(item => item.checked)
        .map(item => item.value);
}