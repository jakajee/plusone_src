import React, { Component } from 'react';
import FaIcon from '../../../app/components/util/FaIcon';

class SearchBarBase extends Component {

    render() {
        return (

            <form onSubmit={e => {
                e.preventDefault();
                this.props.onSubmit();
            }}>
                <div className="row">
                    <div className="form-group col-md-12 col-lg-12">
                        <label>{this.props.label || 'หัวข้อ'}</label>

                        <div className="input-group">
                            <input type="text" className="form-control"
                                value={this.props.value}
                                onChange={this.props.onInputChange} />

                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary" type="submit">
                                    <FaIcon name="fa-search" /> ค้นหา
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        )
    }
}

export default SearchBarBase;