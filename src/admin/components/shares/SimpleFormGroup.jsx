import React from 'react';

export default function SimpleFormGroup(props) {
    return (
        <div className={`form-group ${props.className}`}>
            <label htmlFor={props.inputId} className={props.labelClassName}>{props.label}</label>
            {props.children}
        </div>
    );
}

SimpleFormGroup.defaultProps = {
    className: '',
    labelClassName: '',
    label: '',
    inputId: ''
}