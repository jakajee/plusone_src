import React from 'react';
import SimpleFormGroup from '../../shares/SimpleFormGroup';
import FaIcon from '../../../../app/components/util/FaIcon';

export default class CourtDetailImageList extends React.Component {
    state = {
        imageUrl: ""
    }

    render() {
        return (
            <React.Fragment>
                <div className="form-row">
                    <div className="col-md-12">

                        <div className="form-inline">

                            <SimpleFormGroup label="URL รูปตัวอย่างสนาม">
                            </SimpleFormGroup>

                            <SimpleFormGroup className="ml-1 w-50">
                                <textarea className="form-control resize-none w-100"
                                    onKeyPress={event => event.which === 13 || event.keyCode === 13 ? this.onAddUrl() : ""}
                                    onChange={event => this.setState({ imageUrl: event.target.value })}
                                ></textarea>
                            </SimpleFormGroup>

                            <SimpleFormGroup className="ml-1">
                                <button className="btn btn-success btn-sm" onClick={this.onAddUrl} disabled={!this.state.imageUrl}>
                                    <FaIcon name="fa-plus" /> เพิ่มรูป
                                </button>
                            </SimpleFormGroup>
                        </div>

                    </div>

                </div>

                <div className="form-row mt-3">
                    <div className="col-md-12">
                        <table className="table table-sm table-bordered table-hoverable">
                            <thead>
                                <tr>
                                    <th className="text-center" style={{ width: "50px" }}>#</th>
                                    <th>Url</th>
                                    <th style={{ width: "100px" }}></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.renderCourtDetailImageListItem()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    onAddUrl = () => {
        const imageUrl = this.state.imageUrl;
        this.setState({
            imageUrl: ""
        }, function () {
            this.props.onChange(this.props.courtImageList.concat([{ imageUrl }]));
        })

    }


    renderCourtDetailImageListItem = () => {
        return this.props.courtImageList.map((data, index) => {
            return (
                <CourtDetailImageListItem
                    onDelete={this.onDelete}
                    onUpdateUrl={this.onUpdateUrl}
                    data={data}
                    index={index}
                    key={index}
                />
            );
        })
    }

    onDelete = targetIndex => {
        this.props.onChange(this.props.courtImageList.filter((item, index) => index !== targetIndex));
    }

    onUpdateUrl = (index, value) => {
        let updatedCourt = this.props.courtImageList.slice();
        updatedCourt.splice(index, 1, {
            imageUrl: value
        });

        this.props.onChange(updatedCourt);
    }
}

function CourtDetailImageListItem(props) {
    return (
        <tr>
            <td className="text-center">{props.index + 1}</td>
            <td>
                <textarea className="form-control form-control-sm resize-none" rows="3"
                    value={props.data.imageUrl}
                    onChange={onUpdate}
                ></textarea>
            </td>
            <td>
                <button className="btn btn-sm btn-danger btn-block" onClick={onDelete}>
                    <FaIcon name="fa-times-circle" label="ลบ" />
                </button>
            </td>
        </tr>
    )

    function onUpdate(event) {
        props.onUpdateUrl(props.index, event.target.value);
    }

    function onDelete() {
        props.onDelete(props.index)
    }
}