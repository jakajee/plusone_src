import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import SimpleFormGroup from '../../shares/SimpleFormGroup';
import FaIcon from '../../../../app/components/util/FaIcon';
import Dropdown from '../../../../app/components/util/Dropdown';
import { getAreaList, getProvinceList } from '../../../actions/SystemData';
import { getFacilityList, getSportList } from '../../../actions/MasterData';
import CourtDetailFacility from './CourtDetailFacility';
import { getTimeDatasource } from '../../../../app/components/util/util';
import moment from 'moment';
import CourtDetailImageList from './CourtDetailImageList';
import { getDefaultPleaseSelect } from '../../../util/util';

class CourtDetail extends React.Component {

    state = {
        uuid: this.props.uuid || "",
        systemProvinceId: this.props.systemProvinceId || "",
        systemAreaId: this.props.systemAreaId || "",
        masterSportTypeUuid: this.props.masterSportTypeUuid || "",
        facilityList: this.props.facilityList || [],
        courtImageList: this.props.courtImageList || [],
        courtName: this.props.courtName || "",
        courtCount: this.props.courtCount || "",
        courtDescription: this.props.courtDescription || "",
        courtLat: this.props.courtLat || "",
        courtLng: this.props.courtLng || "",
        courtOpenHours: this.props.courtOpenHours || "",
        courtCloseHours: this.props.courtCloseHours || ""
    }

    componentDidMount() {
        this.props.getProvinceList();
        this.props.getAreaList(this.state.systemProvinceId);
        this.props.getFacilityList();
        this.props.getSportList();
    }

    render() {
        return (
            <React.Fragment>
                <h3 className="text-success">{this.props.title}</h3>

                {this.renderCourtDetail()}

                <hr />

                <div className="row">
                    <div className="col-md-12">
                        <Link to="/admin/courts" className="float-left">
                            กลับ
                        </Link>

                        <button className="btn btn-primary btn-sm float-right" type="button"
                            onClick={this.onSubmit}
                            disabled={this.checkDisable()}
                        >
                            <FaIcon name="fa-check" /> ยืนยัน
                        </button>
                    </div>
                </div>


            </React.Fragment>

        )
    }

    checkDisable = () => {
        const requiredData = [
            this.state.courtName,
            this.state.masterSportTypeUuid,
            this.state.courtCount,
            this.state.courtOpenHours,
            this.state.courtCloseHours,
            this.state.courtDescription,
            this.state.systemProvinceId,
            this.state.systemAreaId,
            this.state.courtLat,
            this.state.courtLng
        ]

        return requiredData.some(item => !item);
    }

    onSubmit = () => {
        this.props.onSubmit(this.state);
    }

    handleInputChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    renderCourtDetail = () => {
        return (
            <React.Fragment>

                <div className="form-row">

                    <SimpleFormGroup label="ชื่อสนาม" className="col-md-4" labelClassName="required">
                        <input
                            type="text"
                            className="form-control"
                            name="courtName"
                            value={this.state.courtName}
                            maxLength="100"
                            onChange={this.handleInputChange} />
                    </SimpleFormGroup>

                    <SimpleFormGroup label="ประเภทกีฬา" className="col-md-2" labelClassName="required">
                        <Dropdown
                            className="form-control"
                            datasource={this.props.masterSportList}
                            value={this.state.masterSportTypeUuid}
                            onChange={event => this.setState({ masterSportTypeUuid: event.target.value })}
                        />
                    </SimpleFormGroup>

                    <SimpleFormGroup label="จำนวนสนาม" className="col-md-2" labelClassName="required">
                        <input
                            type="number"
                            className="form-control"
                            name="courtCount"
                            value={this.state.courtCount}
                            max="100"
                            onChange={this.handleInputChange} />
                    </SimpleFormGroup>

                    <SimpleFormGroup label="เวลาเปิด" className="col-md-2" labelClassName="required">
                        <Dropdown
                            className="form-control"
                            datasource={[getDefaultPleaseSelect()].concat(getTimeDatasource(moment().format('YYYY-MM-DD'), true))}
                            onChange={event => this.setState({ courtOpenHours: event.target.value })}
                            value={this.state.courtOpenHours}
                        />
                    </SimpleFormGroup>
                    <SimpleFormGroup label="เวลาปิด" className="col-md-2" labelClassName="required">

                        <Dropdown
                            className="form-control"
                            datasource={[getDefaultPleaseSelect()].concat(getTimeDatasource(moment().format('YYYY-MM-DD'), true))}
                            onChange={event => this.setState({ courtCloseHours: event.target.value })}
                            value={this.state.courtCloseHours}
                        />
                    </SimpleFormGroup>
                </div>

                <div className="form-row">
                    <SimpleFormGroup label="รายละเอียด" className="col-md-8" labelClassName="required">
                        <textarea rows="3" className="form-control" maxLength="100" style={{ resize: "none" }}
                            name="courtDescription"
                            value={this.state.courtDescription}
                            onChange={this.handleInputChange}
                        ></textarea>
                    </SimpleFormGroup>
                    {this.renderCourtFacility()}
                </div>

                <div className="form-row">
                    <SimpleFormGroup label="จังหวัด" className="col" labelClassName="required">
                        <Dropdown
                            className="form-control"
                            datasource={this.props.systemProvinceList}
                            value={this.state.systemProvinceId}
                            onChange={this.handleChangeProvince}
                        />
                    </SimpleFormGroup>

                    <SimpleFormGroup label="เขต/อำเภอ" className="col" labelClassName="required">
                        <Dropdown
                            className="form-control"
                            datasource={this.props.systemAreaList}
                            value={this.state.systemAreaId}
                            onChange={event => this.setState({ systemAreaId: event.target.value })}
                            disabled={!this.state.systemProvinceId}
                        />
                    </SimpleFormGroup>

                    <SimpleFormGroup label="latitude" className="col" labelClassName="required">
                        <input
                            name="courtLat"
                            type="text"
                            className="form-control"
                            value={this.state.courtLat}
                            maxLength="100"
                            onChange={this.handleInputChange} />
                    </SimpleFormGroup>

                    <SimpleFormGroup label="longitude" className="col" labelClassName="required">
                        <input
                            name="courtLng"
                            type="text"
                            className="form-control"
                            value={this.state.courtLng}
                            maxLength="100"
                            onChange={this.handleInputChange} />
                    </SimpleFormGroup>
                </div>

                {this.renderImagesControl()}


            </React.Fragment>
        )
    }

    handleChangeProvince = (event) => {
        this.setState({
            systemProvinceId: event.target.value,
            systemAreaId: ""
        }, function () {
            this.props.getAreaList(this.state.systemProvinceId);
        })
    }

    renderCourtFacility = () => {
        return (
            <CourtDetailFacility
                facilityList={this.state.facilityList.length === 0
                    ? this.props.masterFacilityList.map(item => ({ ...item, checked: item.checked || false }))
                    : this.state.facilityList}
                onChange={this.handleChangeFacility}
            />
        )
    }

    handleChangeFacility = (facilityList) => {
        this.setState({
            facilityList: [...facilityList]
        })
    }

    renderImagesControl = () => {
        return (
            <CourtDetailImageList
                courtImageList={this.state.courtImageList}
                onChange={this.handleChangeImageList}
            />
        )
    }

    handleChangeImageList = courtImageList => {
        this.setState({
            courtImageList: [...courtImageList]
        })
    }

}

function mapStateToProps(state) {
    return {
        systemProvinceList: state.systemProvinceList,
        systemAreaList: state.systemAreaList,
        masterSportList: state.masterSportList,
        masterFacilityList: state.masterFacilityList
    }
}



const connected = connect(mapStateToProps, { getProvinceList, getAreaList, getSportList, getFacilityList })(CourtDetail)

export default connected;