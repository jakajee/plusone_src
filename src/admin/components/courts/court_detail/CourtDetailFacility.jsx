import React from 'react';
import SimpleFormGroup from '../../shares/SimpleFormGroup';

export default class CourtDetailFacility extends React.Component {

    render() {
        return (
            <React.Fragment>
                <SimpleFormGroup label="สิ่งอำนวยความสะดวก" className="col-md-4">
                    {this.renderCheckboxFacility()}
                </SimpleFormGroup>
            </React.Fragment>
        );
    }

    handleSelectFacility = (value, checked) => {
        this.props.onChange(this.props.facilityList.map(item => {
            return {
                text: item.text,
                value: item.value,
                checked: item.value === value ? checked : item.checked
            }
        }));
    }

    renderCheckboxFacility = () => {
        return this.props.facilityList.map((item, index) => {
            const chkId = `chkFacility_${index}`;
            return (
                <Checkbox
                    key={index}
                    id={chkId}
                    {...item}
                    onChange={this.handleSelectFacility}
                />
            )
        });
    }
}

function Checkbox(props) {
    return (
        <div className="form-check">
            <input className="form-check-input" name="facilityList" type="checkbox" id={props.id} value={props.value}
                checked={props.checked}
                onChange={handleChange}
            />
            <label htmlFor={props.id}>{props.text}</label>
        </div>
    )

    function handleChange() {
        props.onChange(props.value, !props.checked)
    }
}

