import React from 'react';
import { Link } from 'react-router-dom';


const CourtListTableItem = (props) => {
    return (
        <tr>
            <td>{props.no}</td>
            <td>
                <Link to={`/admin/courts/${props.uuid}`}>                    
                    {props.courtName}
                </Link>
            </td>
            <td>{props.tblMasterSportType.sportName}</td>
        </tr>
    )
};

export default CourtListTableItem;