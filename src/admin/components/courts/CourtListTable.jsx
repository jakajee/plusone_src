import React, { Component } from 'react';
import CourtListTableItem from './CourtListTableItem';
import ReducerSubScription from '../../../others/shares/ReducerSubScription';
import NotFound from '../../../app/components/util/NotFound';
import TableResponsive from '../../../others/shares/Table/TableResponsive';
import TableContainer from '../../../others/shares/Table/TableContainer';
import TableHeader from '../../../others/shares/Table/TableHeader';
import TableBody from '../../../others/shares/Table/TableBody';

class CourtListTable extends Component {
    render() {

        return (
            <TableResponsive>
                <TableContainer>

                    <TableHeader>
                        <tr>
                            <th>#</th>
                            <th>ชื่อสนาม</th>
                            <th>ประเภทกีฬา</th>
                        </tr>
                    </TableHeader>

                    <TableBody>
                        {this.renderCourtListTableItem()}
                    </TableBody>

                </TableContainer>
            </TableResponsive>
        )
    }

    renderCourtListTableItem() {
        const { masterCourtList } = this.props;

        if (!masterCourtList || masterCourtList.length === 0)
            return (
                <tr>
                    <td colSpan="3">
                        <NotFound alertMessage="ไม่พบสนาม" />
                    </td>
                </tr>
            )
        return masterCourtList.map((item, index) =>
            <CourtListTableItem {...item} no={index + 1} key={item.uuid} />
        )

    }
}

export default ReducerSubScription(CourtListTable, "masterCourtList");