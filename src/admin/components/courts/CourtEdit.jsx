import React from 'react';
import { connect } from 'react-redux';

import { getCourtDetail, updateCourt } from '../../actions/MasterCourt';
import CourtDetail from './court_detail/CourtDetail';
import contentPlaceHolder from '../../../app/components/util/ContentPlaceHolder';

class CourtEdit extends React.Component {


    componentDidMount() {
        this.props.getCourtDetail(
            this.props.match.params.id
        );
    }

    render() {
        const CourtDetailWithLoading = contentPlaceHolder(
            CourtDetail,
            () => !!this.props.courtDetail.uuid
        )

        return (
            <div>
                <CourtDetailWithLoading 
                    {...this.props.courtDetail}
                    onSubmit={this.onUpdate}
                    title={`แก้ไขสนาม: ${this.props.courtDetail.courtName}`}
                />
            </div>
        )
    }

    onUpdate = (model) => {
        console.log(model);

        updateCourt(model)
            .then(() => this.props.history.push('/admin/courts'));
    }
}

function mapStateToProps(state) {
    return {
        courtDetail: state.courtDetail
    }
}

export default connect(mapStateToProps, { getCourtDetail })(CourtEdit);