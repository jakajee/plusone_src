import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';

import Dropdown from '../../../app/components/util/Dropdown';
import SimpleFormGroup from '../shares/SimpleFormGroup';
import useDropDownSportTypeList from '../../util/hooks/useDropDownSportTypeList';
import FaIcon from '../../../app/components/util/FaIcon';
import useMasterCourtList from '../../util/hooks/useMasterCourtList';

const CourtSearchModal = props => {
    const [courtName, setCourtName] = useState('');
    const [sportTypeUuid, setSportTypeUuid] = useState('');
    const [selectedCourtUuid, setSelectedCourtUuid] = useState('');
    const [selectedCourtName, setSelectedCourtName] = useState('');
    const [sportList] = useDropDownSportTypeList();
    const [courtList, setCourtList] = useMasterCourtList();

    return (
        <Modal show={props.showModal} onHide={props.handleHide} size="md">
            <Modal.Header closeButton>
                <Modal.Title>
                    <strong>ค้นหาสนาม</strong>
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {renderSearch()}
                {renderSearchResult()}
            </Modal.Body>

            <Modal.Footer>
                <button type="button" className="btn btn-secondary text-white" onClick={props.handleHide}>ปิด</button>
                <button type="button" className="btn btn-success" disabled={!selectedCourtUuid} onClick={onSubmitModal}>ยืนยัน</button>
            </Modal.Footer>
        </Modal>
    )

    function renderSearch() {
        return (
            <div className="row">
                <SimpleFormGroup label="ประเภทกีฬา" className="col-md-4">
                    <Dropdown
                        className="form-control"
                        datasource={sportList}
                        value={sportTypeUuid}
                        onChange={event => {
                            setCourtList([]);
                            setSelectedCourtName('');
                            setSelectedCourtUuid('');
                            setSportTypeUuid(event.target.value);
                        }}
                    />

                </SimpleFormGroup>

                <SimpleFormGroup label="ชื่อสนาม" className="col-md-8">
                    <div className="input-group">
                        <input type="text"
                            className="form-control"
                            value={courtName}
                            onChange={e => setCourtName(e.target.value)}
                            onKeyPress={e => e.which === 13 || e.keyCode === 13 ? fetchCourt() : null}
                        />
                        <div className="input-group-append">
                            <button type="button" className="btn btn-outline-secondary" onClick={e => fetchCourt()}>
                                <FaIcon name="fa-search" />
                            </button>
                        </div>
                    </div>

                </SimpleFormGroup>
            </div>
        )
    }

    function renderSearchResult() {
        return (
            <div className="row">
                <div className="col-md-12">
                    <ul className="list-group">
                        {courtList.map(item => (
                            <li key={item.uuid} className={`list-group-item pointer ${item.uuid === selectedCourtUuid ? 'active' : ''}`}
                                onClick={e => {
                                    setSelectedCourtUuid(item.uuid);
                                    setSelectedCourtName(item.courtName);
                                }}
                                onDoubleClick={e => {
                                    setSelectedCourtUuid(item.uuid);
                                    setSelectedCourtName(item.courtName);
                                    onSubmitModal();
                                }}
                            >
                                {item.courtName}
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        )
    }

    function onSubmitModal() {
        if (!selectedCourtUuid) return;
        
        props.onSubmit({ selectedCourtName, selectedCourtUuid, sportTypeUuid })
        props.handleHide();
    }

    function fetchCourt() {
        setSelectedCourtName('');
        setSelectedCourtUuid('');
        setCourtList(sportTypeUuid, courtName);
    }
}

export default CourtSearchModal;