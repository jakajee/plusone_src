import React, { Component } from 'react';
import { connect } from 'react-redux';

import SearchBarBase from '../shares/SearchBarBase';
import { getMasterCourtList } from '../../actions/MasterCourt';

class CourtListSearch extends Component {
    state = {
        value: ''
    }

    onSearchCourt = () => {
        this.props.getMasterCourtList({
            courtName: this.state.value
        })
    }

    render() {
        return (
            <SearchBarBase
                value={this.state.value}
                label="ชื่อสนาม"
                onInputChange={e => this.setState({ value: e.target.value })}
                onSubmit={this.onSearchCourt}
            />
        )
    }
}

const connectedComponent = connect(null, { getMasterCourtList })(CourtListSearch);

export default connectedComponent;