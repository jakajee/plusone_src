import React from 'react';
import CourtListSearch from './CourtListSearch';
import CourtListTable from './CourtListTable';
import { Link } from 'react-router-dom';
import FaIcon from '../../../app/components/util/FaIcon';

const CourtListMain = () => {
    return (
        <React.Fragment>
            <div className="row mb-2">
                <div className="col-2">
                    <Link to="/admin/courts/add" className="btn btn-primary btn-sm">
                        <FaIcon name="fa-plus" /> เพิ่มสนาม
                        </Link>
                </div>
            </div>
            <CourtListSearch />
            <CourtListTable />
        </React.Fragment>
    )
}

export default CourtListMain;