import React from 'react';
import CourtDetail from './court_detail/CourtDetail';
import { createCourt } from '../../actions/MasterCourt';

export default class CourtAdd extends React.Component {
    render() {
        return (
            <React.Fragment>
                <CourtDetail
                    onSubmit={this.onCreate}
                    title="เพิ่มสนาม"
                />
            </React.Fragment>
        )
    }

    onCreate = (model) => {
        createCourt(model)
            .then(() => this.props.history.push('/admin/courts'));
    }
}