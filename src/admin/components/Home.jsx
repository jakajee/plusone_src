import React from 'react';
import { Link } from 'react-router-dom';

export default function Home(props) {

    return (
        <div className="row">
            {renderLink('เพิ่มสนาม', '/admin/courts/add')}
            {renderLink('เพิ่มกิจกรรม', '/admin/events/add')}
        </div>
    )

    function renderLink(title, url) {
        return (
            <div className="col">
                <Link className="btn btn-primary btn-lg btn-block text-center" to={url}>
                    {title}
                </Link>
            </div>
        )
    }
    
}