import React from 'react';
import { Link } from 'react-router-dom';
import TableResponsive from '../../../others/shares/Table/TableResponsive';
import TableContainer from '../../../others/shares/Table/TableContainer';
import TableHeader from '../../../others/shares/Table/TableHeader';
import TableBody from '../../../others/shares/Table/TableBody';
import ReducerSubScription from '../../../others/shares/ReducerSubScription';
import NotFound from '../../../app/components/util/NotFound';
import DateTimeFormat from '../../../others/shares/DateTimeFormat';

class EventListTable extends React.Component {
    render() {
        return (
            <TableResponsive>
                <TableContainer>
                    <TableHeader>
                        <tr>
                            <th>#</th>
                            <th>ชื่อกิจกรรม</th>
                            <th>สนาม</th>
                            <th>ประเภทกีฬา</th>
                            <th>วันที่เริ่ม</th>
                            <th>วันที่สิ้นสุด</th>
                            <th>วันที่สร้าง</th>
                            <th>วันที่แก้ไข</th>
                        </tr>
                    </TableHeader>
                    <TableBody>
                        {this.renderEventListItem()}
                    </TableBody>
                </TableContainer>
            </TableResponsive>
        )
    }

    renderEventListItem = () => {
        
        if (this.props.masterEventList.length === 0) return <tr><td colSpan="8" className="text-center"><NotFound alertMessage="ไม่พบกิจกรรม" /></td></tr>;
        const dateTimeFormat = 'DD/MM/YYYY HH:mm';
        const dateTimeFormatFull = `${dateTimeFormat}:ss`

        return this.props.masterEventList.map((item, index) => {
            return (
                <tr key={item.uuid} className={item.flagChange ? 'bg-success': ''}>
                    <td>{index + 1}</td>
                    <td>
                        <Link to={`/admin/events/${item.uuid}`}>
                            {item.eventName}
                        </Link>
                    </td>
                    <td>{item.tblMasterCourt.courtName}</td>
                    <td>{item.tblMasterSportType.sportName}</td>
                    <td>
                        <DateTimeFormat value={item.eventDateTimeStart} targetFormat={dateTimeFormat} />
                    </td>
                    <td>
                        <DateTimeFormat value={item.eventDateTimeEnd} targetFormat={dateTimeFormat} />
                    </td>
                    <td>
                        <DateTimeFormat value={item.dateTimeCreated} targetFormat={dateTimeFormatFull} />
                    </td>
                    <td>
                        <DateTimeFormat value={item.dateTimeModified} targetFormat={dateTimeFormatFull} />
                    </td>
                </tr>
            )
        })
    }
}

export default ReducerSubScription(EventListTable, "masterEventList");