import React from 'react';
import { getMasterEventByUuid, updateEvent } from '../../actions/MasterEvent';
import EventDetail from './event_detail/EventDetail';
import contentPlaceHolder from '../../../app/components/util/ContentPlaceHolder';
import ActionDispatcher from '../../../others/shares/ActionDispatcher';

class EventEdit extends React.Component {
    state = {
        eventModel: null
    }

    async componentDidMount() {
        const { id } = this.props.match.params;
        const eventModel = await getMasterEventByUuid(id);
        this.setState({
            eventModel
        })
    }

    render() {
        const WrappedEventDetail = contentPlaceHolder(EventDetail, () => !!this.state.eventModel);
        const { eventName } = this.state.eventModel || {};

        return (
            <React.Fragment>
                <WrappedEventDetail
                    title={`แก้ไขกิจกรรม: ${eventName}`}
                    onSubmit={this.onSubmit}
                    {...this.state.eventModel}
                />
            </React.Fragment>
        )
    }

    onSubmit = (updatedModel) => {
        this.props.updateEvent({
            ...this.state.eventModel,
            ...updatedModel
        })
    }
}

export default ActionDispatcher(EventEdit, { updateEvent });