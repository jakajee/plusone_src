import React from 'react';
import SearchBarBase from '../shares/SearchBarBase';
import ActionDispatcher from '../../../others/shares/ActionDispatcher';
import { getMasterEventList } from '../../actions/MasterEvent';

class EventListSearch extends React.Component {
    state = {
        eventName: ''
    }

    onSearchEvent = () => {
        const { eventName } = this.state;
        this.props.getMasterEventList({ eventName });
    }

    render() {
        return (
            <React.Fragment>
                <SearchBarBase
                    value={this.state.eventName}
                    label="ชื่อกิจกรรม"
                    onInputChange={e => this.setState({ eventName: e.target.value })}
                    onSubmit={this.onSearchEvent}
                />
            </React.Fragment>
        )
    }
}

export default ActionDispatcher(EventListSearch, { getMasterEventList });