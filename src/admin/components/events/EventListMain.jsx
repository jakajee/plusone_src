import React from 'react';
import FaIcon from '../../../app/components/util/FaIcon';
import { Link } from 'react-router-dom';
import EventListSearch from './EventListSearch';
import EventListTable from './EventListTable';


const EventListMain = () => {
    return (
        <React.Fragment>
            <div className="row mb-2">
                <div className="col-2">
                    <Link to="/admin/events/add" className="btn btn-primary btn-sm">
                        <FaIcon name="fa-plus" /> เพิ่มกิจกรรม
                        </Link>
                </div>
            </div>
            
            <EventListSearch />
            <EventListTable />
        </React.Fragment>
    )
}

export default EventListMain;