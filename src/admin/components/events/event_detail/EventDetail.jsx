import React from 'react';
import { Link } from 'react-router-dom';

import DatePicker from '../../../../app/components/util/DatePicker';
import SimpleFormGroup from '../../shares/SimpleFormGroup';
import FaIcon from '../../../../app/components/util/FaIcon';
import CourtSearchModal from '../../courts/CourtSearchModal';
import DateTimeFormat from '../../../../others/shares/DateTimeFormat';

const defaultDateConfig = {
    enableTime: true,
    allowInput: false,
    minuteIncrement: 1,
    time_24hr: true
}

class EventDetail extends React.Component {
    state = {
        eventName: this.props.eventName || '',
        eventFee: this.props.eventFee || '',
        eventDescription: this.props.eventDescription || '',
        eventReward: this.props.eventReward || '',
        eventDateTimeStart: this.props.eventDateTimeStart || '',
        eventDateTimeEnd: this.props.eventDateTimeEnd || '',
        eventImg: this.props.eventImg || '',
        matchCapability: this.props.matchCapability || '',
        courtName: this.props.courtName || '',
        masterSportTypeUuid: this.props.masterSportTypeUuid || '',
        masterCourtUuid: this.props.masterCourtUuid || '',
        showModal: false
    }

    render() {
        return (
            <React.Fragment>
                <h3 className="text-success">{this.props.title}</h3>
                {this.renderUserLog()}
                {this.renderEventDetail()}

                <CourtSearchModal
                    showModal={this.state.showModal}
                    handleHide={() => this.setState({ showModal: !this.state.showModal })}
                    onSubmit={this.handleSelectCourt}
                />

                <div className="row">
                    <div className="col-md-12">
                        <Link to="/admin/events" className="float-left">
                            กลับ
                        </Link>

                        <button className="btn btn-primary btn-sm float-right" type="button"
                            onClick={this.onSubmit}
                            disabled={this.checkDisable()}
                        >
                            <FaIcon name="fa-check" /> ยืนยัน
                        </button>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    checkDisable = () => {
        const requiredField = [
            this.state.eventName,
            this.state.eventFee,
            this.state.eventDescription,
            this.state.eventReward,
            this.state.eventDateTimeStart,
            this.state.eventDateTimeEnd,
            this.state.eventImg,
            this.state.masterCourtUuid,
            this.state.masterSportTypeUuid,
            this.state.matchCapability
        ];

        return requiredField.some(item => !item);
    }

    onSubmit = () => {
        this.props.onSubmit({ ...this.state });
    }

    handleInputChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleDateChange = stateName => (date, dateStr) => {
        this.setState({
            [stateName]: dateStr
        });
    }

    handleSelectCourt = ({ selectedCourtName, selectedCourtUuid, sportTypeUuid }) => {
        this.setState({
            courtName: selectedCourtName,
            masterCourtUuid: selectedCourtUuid,
            masterSportTypeUuid: sportTypeUuid
        });
    }

    renderEventDetail = () => {
        return (
            <React.Fragment>
                <div className="row">
                    <SimpleFormGroup label="ชื่อกิจกรรม" className="col-md-5" labelClassName="required">
                        <input
                            className="form-control"
                            type="text"
                            name="eventName"
                            value={this.state.eventName}
                            maxLength="255"
                            onChange={this.handleInputChange}
                        />
                    </SimpleFormGroup>

                    <SimpleFormGroup label="สนาม" className="col-md-3" labelClassName="required">
                        <div className="input-group">
                            <input
                                type="text"
                                className="form-control"
                                disabled
                                value={this.state.courtName}
                            />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary" type="button" onClick={e => this.setState({ showModal: !this.state.showModal })} >
                                    <FaIcon name="fa-search" />
                                </button>
                            </div>
                        </div>
                    </SimpleFormGroup>

                    <SimpleFormGroup label="จำนวนคน" className="col-md-2" labelClassName="required">
                        <input
                            type="number"
                            className="form-control"
                            name="matchCapability"
                            value={this.state.matchCapability}
                            onChange={this.handleInputChange} />
                    </SimpleFormGroup>

                    <SimpleFormGroup label="ค่าธรรมเนียม" className="col-md-2" labelClassName="required">
                        <input
                            type="number"
                            className="form-control"
                            name="eventFee"
                            value={this.state.eventFee}
                            onChange={this.handleInputChange} />
                    </SimpleFormGroup>
                </div>

                <div className="row">
                    <SimpleFormGroup label="รายละเอียด" className="col-md-12" labelClassName="required">
                        <textarea
                            className="form-control resize-none"
                            name="eventDescription"
                            rows="3"
                            value={this.state.eventDescription}
                            onChange={this.handleInputChange}
                        >
                        </textarea>
                    </SimpleFormGroup>
                </div>

                <div className="row">
                    <SimpleFormGroup label="รางวัล" className="col-md-6" labelClassName="required">
                        <input
                            className="form-control"
                            name="eventReward"
                            type="text"
                            maxLength="100"
                            value={this.state.eventReward}
                            onChange={this.handleInputChange}
                        />
                    </SimpleFormGroup>
                    <SimpleFormGroup label="วันที่เริ่ม" className="col-md-3" labelClassName="required">
                        <DatePicker
                            className="form-control bg-white"
                            placeholder="YYYY-MM-dd HH:mm"
                            value={this.state.eventDateTimeStart}
                            onChange={this.handleDateChange("eventDateTimeStart")}
                            options={{ ...defaultDateConfig }}
                        />
                    </SimpleFormGroup>
                    <SimpleFormGroup label="วันที่สิ้นสุด" className="col-md-3" labelClassName="required">
                        <DatePicker
                            className="form-control bg-white"
                            placeholder="YYYY-MM-dd HH:mm"
                            value={this.state.eventDateTimeEnd}
                            onChange={this.handleDateChange("eventDateTimeEnd")}
                            options={{ ...defaultDateConfig }}
                        />
                    </SimpleFormGroup>
                </div>

                <div className="row">
                    <SimpleFormGroup label="Url รูป Promote" className="col-md-12" labelClassName="required">
                        <input
                            type="text"
                            className="form-control"
                            name="eventImg"
                            value={this.state.eventImg}
                            onChange={this.handleInputChange}
                        />
                    </SimpleFormGroup>
                </div>

                {this.renderImgPreview()}

            </React.Fragment>
        )
    }

    renderUserLog = () => {
        const {
            userCreated,
            userModified,
            dateTimeCreated,
            dateTimeModified
        } = this.props;
        const dateTimeFormat = 'DD/MM/YYYY HH:mm:ss';

        if (!userCreated) return null;

        const renderLogInfo = (faIcon, lbl, value1, value2) => {
            if (!value1) return null;

            return (
                <React.Fragment>
                    <i className={`fas ${faIcon}`} style={{ marginRight: "2px" }}></i>
                    <strong className="mr-1">{lbl}</strong>
                    <label className="mr-1">{value1}</label>
                    <strong className="mr-1">เวลา</strong>
                    <label><DateTimeFormat value={value2} targetFormat={dateTimeFormat} /></label>
                </React.Fragment>
            )
        }

        return (
            <h6>
                {renderLogInfo('fa-plus-circle', 'สร้างโดย', userCreated, dateTimeCreated)}
                <span className="mr-1 ml-1"></span>
                {renderLogInfo('fa-edit', 'แก้ไขล่าสุดโดย', userModified, dateTimeModified)}
            </h6>
        )
    }

    renderImgPreview = () => {
        if (!this.state.eventImg)
            return null;

        return (
            <div className="row">
                <SimpleFormGroup label="ตัวอย่างรูป" className="col-md-12">
                    <div>
                        <img src={this.state.eventImg} width="120" height="120" className="img-thumbnail" />
                    </div>
                </SimpleFormGroup>
            </div>
        )
    }
}

export default EventDetail;