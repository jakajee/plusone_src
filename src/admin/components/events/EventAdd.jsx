import React from 'react';

import EventDetail from './event_detail/EventDetail';
import { createEvent } from '../../actions/MasterEvent';
import ActionDispatcher from '../../../others/shares/ActionDispatcher';

class EventAdd extends React.Component {
    onSubmit = (model) => {
        this.props.createEvent(model);
    }

    render() {
        return (
            <React.Fragment>
                <EventDetail
                    title="เพิ่มกิจกรรม"
                    onSubmit={this.onSubmit}
                />
            </React.Fragment>
        )
    }
}

export default ActionDispatcher(EventAdd, { createEvent });