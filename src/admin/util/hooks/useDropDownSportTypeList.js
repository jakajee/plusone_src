import React, { useState, useEffect } from 'react';
import { getSportListAsync } from '../../actions/MasterData';

let abortController = new AbortController();

const useDropDownSportTypeList = () => {
    const [sportList, setSportList] = useState([]);

    async function fetchSportList() {
        const sportTypeList = await getSportListAsync();
        setSportList(sportTypeList.map(data => ({
            text: data.sportName,
            value: data.uuid
        })));
    }

    useEffect(() => {
        fetchSportList();
    }, [])

    return [sportList];
}

export default useDropDownSportTypeList;