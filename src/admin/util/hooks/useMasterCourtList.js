import React, { useState } from 'react';
import { getCourtListAsync } from '../../actions/MasterData';

const useMasterCourtList = () => {
    const [courtList, setCourtList] = useState([]);

    async function fetchMasterCourt(sportTypeUuid, courtName) {
        const courtList = await getCourtListAsync({sportTypeUuid, courtName});
        setCourtList([...courtList]);
    }

    return [courtList, fetchMasterCourt];
}

export default useMasterCourtList;