
export const getDefaultPleaseSelect = (textLabel, valueLabel) => ({
    [textLabel || "text"]: "โปรดเลือก",
    [valueLabel || "value"]: ""
})