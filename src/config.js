export const API_PATH = process.env.NODE_ENV === "development" ? "http://localhost:3000/api" : "http://plusone.ap-southeast-1.elasticbeanstalk.com/api";
export const API_PATH_V1 = `${API_PATH}/v1`;
export const API_PATH_ADMIN = `${API_PATH}/datamanagement`;