import axios from 'axios';
import { API_PATH_ADMIN, API_PATH_V1, API_PATH } from '../config';
import { setToken } from '../app/infrastructure/interceptors';

const createAxiosInstance = baseURL => {
    const axiosInstance = axios.create({
        baseURL
    });

    axiosInstance.interceptors.request.use((config) => setToken(config));
    return axiosInstance;
}


export const plusoneApp = createAxiosInstance(API_PATH_V1);
export const pluseoneAdmin = createAxiosInstance(API_PATH_ADMIN);
export const plusoneRoot = createAxiosInstance(API_PATH);

