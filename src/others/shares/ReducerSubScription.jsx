import React, { Component } from 'react';
import { connect } from 'react-redux';

function ReducerSubScription(WrappedComponent, stateName) {
    class ReducerSubScriptionWrapper extends Component {
        render() {
            return <WrappedComponent {...this.props} />
        }
    }

    function mapStateToProps(state) {
        return { [stateName]: state[stateName] };
    }

    return connect(mapStateToProps)(ReducerSubScriptionWrapper);
}

export function ReducerSubScriptionWithMapStateToProps(WrappedComponent, mapStateToProps) {
    class ReducerSubScriptionWrapper extends Component {
        render() {
            return <WrappedComponent {...this.props} />
        }
    }
    
    return connect(mapStateToProps)(ReducerSubScriptionWrapper);
}

export default ReducerSubScription;