import React from 'react';
import moment from 'moment';

export const DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';

export default ({value, srcFormat = DATE_TIME_FORMAT, targetFormat = DATE_TIME_FORMAT}) => {
    if(!value) return '';

    const date = moment(value, srcFormat).format(targetFormat);
    return <span>{date}</span>
}