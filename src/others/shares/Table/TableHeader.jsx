import React from 'react';

export default props => {
    return (
        <thead className="thead-dark">
            {props.children}
        </thead>
    )
}