import React from 'react';

export default props => {
    return (
        <table className="table table-sm table-bordered table-hover">
            {props.children}
        </table>
    )
}