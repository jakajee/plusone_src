import React from 'react';

export default props => {
    return (
        <tbody>
            {props.children}
        </tbody>
    )
}