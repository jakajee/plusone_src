import React from 'react';

export default props => {
    return (
        <div className="table-responsive-md">
            {props.children}
        </div>
    )
}