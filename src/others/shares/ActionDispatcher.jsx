import React from 'react';
import { connect } from 'react-redux';

const ActionDispatcher = (WrappedComponent, actions) => {
    class ActionDispatcherWrapper extends React.Component {
        render() {
            return <WrappedComponent {...this.props}  />
        }
    }

    return connect(null, {...actions})(ActionDispatcherWrapper);
}

export default ActionDispatcher;