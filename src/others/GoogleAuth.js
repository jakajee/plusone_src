
class GoogleAuth {

    auth = null;

    onLoadgapi = (resolve, reject) => () => {
        window.gapi.client
            .init({
                clientId: '450839483619-j3cmd52rbggcjlnptk59u4j7dkit6e8o.apps.googleusercontent.com',
                scope: 'profile'
            })
            .then(() => {
                this.auth = window.gapi.auth2.getAuthInstance();
                resolve(this.auth);
            }, reject)
    }

    getCurrentAuth = () => {
        const promise = new Promise((resolve, reject) => {
            if (this.auth) {
                resolve(this.auth);
                return;
            }

            if (!window.gapi) return;

            window.gapi.load("client:auth2", this.onLoadgapi(resolve, reject));
        });

        return promise;
    }
}

export default GoogleAuth;