import { REVIEW_FETCH, REVIEW_CREATE, REVIEW_UPDATE } from "../actions/Review";

export function reviewReducer(state = [], action) {
    switch (action.type) {
        case REVIEW_FETCH: return action.payload.data;
        case REVIEW_CREATE: return reviewCreateReducer(state, action);
        case REVIEW_UPDATE: return reviewUpdateReducer(state, action);
        default: return state;
    }
}


function reviewCreateReducer(state = [], action) {
    if (action.payload.data.flagSuccess) {
        // attach the newly 
        return [{
            reviewId: action.payload.data.data.uuid,
            ...action.reviewModel
        }, ...state
        ];
    } else {
        return state;
    }
}

function reviewUpdateReducer(state = [], action) {
    const reviewId = action.reviewModel.reviewId;

    if (action.payload.data.flagSuccess) {
        // attach the newly 
        return state.map(review =>
            (review.reviewId === reviewId)
                ? { ...action.reviewModel }
                : review
        );
    } else {
        return state;
    }
}