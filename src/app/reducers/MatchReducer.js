import _ from 'lodash';
import { MATCH_GET_BY_ID, MATCH_DETAIL_HIDE, MATCH_LIST, MATCH_JOIN, MATCH_CREATE } from "../actions/Match";

const defaultState = {
    matchDetail: { showModal: false },
    matchList: {}
}

export default function (state = defaultState, action) {
    switch (action.type) {
        case MATCH_GET_BY_ID: return { ...state, matchDetail: { ...action.payload.data, showModal: true } };
        case MATCH_DETAIL_HIDE: return { ...state, matchDetail: { ...defaultState.matchDetail } };
        case MATCH_CREATE:
        case MATCH_LIST: return { ...state, matchList: _.mapKeys(action.payload.data, 'uuid') };
        case MATCH_JOIN: return {
            ...state,
            matchList: {
                ...state.matchList,
                [action.matchUuid]: { ...action.payload.data }
            }
        }
        default: return state;
    }
}