import {
    COURT_SEARCH, COURT_GET_BY_ID
} from "../actions/Court";

export function courtSearchReducer(state = [], action) {

    switch (action.type) {
        case COURT_SEARCH: return action.payload.data;
        default: return state;
    }
}

export function courtGetReducer(state = {}, action) {
    switch (action.type) {
        case COURT_GET_BY_ID: {
            return action.payload.data;
        };
        default: return state;
    }
}