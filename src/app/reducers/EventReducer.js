import _ from 'lodash';
import { GET_EVENT_LIST, SELECT_EVENT_BOARD } from "../actions/Event";


export default (state = {}, action) => {
    switch (action.type) {
        case GET_EVENT_LIST: return {  ...mapToObject(action.payload) };
        case SELECT_EVENT_BOARD: return {
            ...mapToObject(Object.values(state).map(data => _.omit(data, 'selectedInBoard'))),
            [action.payload]: {
                ...state[action.payload],
                selectedInBoard: true
            }
        };
        default: return state;
    }
}

function mapToObject(array) {
    return { ..._.mapKeys(array, 'eventId') };
}