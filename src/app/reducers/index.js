import { combineReducers } from "redux";
import { courtSearchReducer, courtGetReducer } from "./CourtReducer";
import { reviewReducer } from "./ReviewReducer";
import matchReducer from "./MatchReducer";
import eventReducer from "./EventReducer";

const rootReducer = combineReducers({
    courtList: courtSearchReducer,
    courtModel: courtGetReducer,
    reviewList: reviewReducer,    
    matchModel: matchReducer,
    events: eventReducer,
    auth: (state = '') => state
})

export default rootReducer;