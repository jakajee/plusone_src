import React from 'react';
import EventDetailDescription from './EventDetailDescription';
import EventDetailLocation from './EventDetailLocation';

const EventDetailMain = props => {
    const { eventImg } = props;
    return (
        <div className="card">
            <img src={eventImg.imgUrl} className="card-img-top img-thumbnail" width="100%" style={{ height: "350px" }} />
            <div className="card-body">
                <div className="row">
                    <EventDetailDescription {...props} />
                    <EventDetailLocation {...props} />
                </div>

                <div className="row">
                    <div className="col-md-8">
                        <div className="card mb-4">
                            <div className="card-header">
                                <h3>ผู้เข้าร่วม</h3>
                            </div>
                            <div className="card-body">
                                <strong className="text-secondary">
                                    Coming soon...
                                </strong>
                            </div>
                        </div>

                        <div className="card">
                            <div className="card-header">
                                <h3>ทีม</h3>
                            </div>
                            <div className="card-body">
                                <strong className="text-secondary">
                                    Coming soon...
                                </strong>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-4">
                        <div className="card">
                            <div className="card-header">
                                <h3>อันดับผู้เล่น</h3>
                            </div>
                            <div className="card-body">
                                <strong className="text-secondary">
                                    Coming soon...
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default EventDetailMain;