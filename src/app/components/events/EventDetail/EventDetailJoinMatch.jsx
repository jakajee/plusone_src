import React from 'react';
import { isUserInMatch, isMatchFull, joinMatch } from '../../../actions/Match';
import ActionDispatcher from '../../../../others/shares/ActionDispatcher';
import { toast } from 'react-toastify';
import FaIcon from '../../util/FaIcon';

class EventDetailJoinMatch extends React.Component {
    state = {
        isUserInMatch: false,
        isMatchFull: false
    }

    async componentDidMount() {
        const resultIsMatchFull = await isMatchFull(this.props.matchUuid);

        if (resultIsMatchFull) {
            this.setState({
                isMatchFull: resultIsMatchFull
            });
            return;
        }

        const resultCheckUserInMatch = await isUserInMatch(this.props.matchUuid);
        this.setState({
            isMatchFull: resultIsMatchFull,
            isUserInMatch: resultCheckUserInMatch
        })
    }

    render() {
        return (
            <section className="mb-3">
                {this.renderButton()}
            </section>
        )
    }

    renderButton = () => {
        const {
            isUserInMatch,
            isMatchFull
        } = this.state;

        const renderBtn = (text, css, disable) => (
            <button type="button" className={`btn btn-block btn-lg ${css}`} disabled={disable}
                onClick={this.handleClickJoin}>
                    <FaIcon faIconName="fa-plus-square" />
                {text}
            </button>
        );

        if (isMatchFull) {
            return renderBtn('ผู้เข้าแข่งขันเต็มแล้ว', '', true);
        }

        return renderBtn(
            isUserInMatch ? 'คุณเข้าร่วมการแข่งขันนี้แล้ว' : 'เข้าร่วมการแข่งขัน',
            isUserInMatch ? '' : 'btn-danger',
            isUserInMatch
        )
    }

    handleClickJoin = () => {
        this.props.joinMatch(this.props.matchUuid);
        this.setState({
            isUserInMatch: true
        }, () => {
            toast.success('เข้าร่วมการแข่งขันสำเร็จ');
        });
    }
}

export default ActionDispatcher(EventDetailJoinMatch, { joinMatch });