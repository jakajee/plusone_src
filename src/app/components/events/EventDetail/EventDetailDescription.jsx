import React from 'react';

const Content = (props) => {
    return (
        <React.Fragment>
            <h4 className="card-text">
                <strong className="text-secondary">
                    {props.title}
                </strong>
            </h4>
            <p className="lead">
                {props.children}
            </p>
        </React.Fragment>
    )
}

const EventDetailDescription = ({ eventName, eventDescription, eventReward, eventFee }) => {
    return (
        <div className="col-md-8">
            <h1 className="card-title text-primary">
                {eventName}
            </h1>
            <Content title="รายละเอียด">
                {eventDescription}
            </Content>
            <Content title="ค่าสมัคร">
                {eventFee}
            </Content>
            <Content title="รางวัล">
                {eventReward}
            </Content>            
        </div>
    )
}

export default EventDetailDescription;