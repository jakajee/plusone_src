import React from 'react';
import { Link } from 'react-router-dom';
import DateTimeFormat from '../../../../others/shares/DateTimeFormat';
import GoogleMap from '../../maps/GoogleMap';
import MatchDetail from '../../matchs/MatchDetail';
import ActionDispatcher from '../../../../others/shares/ActionDispatcher';
import { getMatchById } from '../../../actions/Match';
import EventDetailJoinMatch from './EventDetailJoinMatch';
import moment from 'moment';

const Content = (props) => {
    return (
        <section className="mb-1">
            <h5 className="text-secondary">{props.title}</h5>
            <h6>
                {props.children}
            </h6>
        </section>
    )
}

class EventDetailLocation extends React.Component {
    render() {
        const targetDateFormat = "DD/MM/YYYY HH:mm";
        const {
            eventDateTimeStart,
            eventDateTimeEnd,
            masterCourtUuid,
            tblMasterCourt,
            tblMasterSportType,
            matchUuid
        } = this.props;
        const eventDate = moment(eventDateTimeStart).format('YYYY-MM-DD');

        return (
            <div className="col-md-4">
                <EventDetailJoinMatch matchUuid={matchUuid} />

                <Content title="สนาม">
                    <Link to={`/app/court_search/${tblMasterSportType.sportId}/${masterCourtUuid}/${tblMasterCourt.courtName}/${eventDate}`}>
                        {tblMasterCourt.courtName}
                    </Link>
                </Content>
                <Content title="วันที่">
                    <a href="javascript:void(0)" onClick={this.handleClickMatchDate.bind(this, matchUuid)}>
                        <DateTimeFormat value={eventDateTimeStart} targetFormat={targetDateFormat} />
                        <span> - </span>
                        <DateTimeFormat value={eventDateTimeEnd} targetFormat={targetDateFormat} />
                    </a>
                </Content>
                
                <MatchDetail />
            </div>
        )
    }

    handleClickMatchDate = (matchUuid) => {
        this.props.getMatchById(matchUuid);
    }
}

export default ActionDispatcher(EventDetailLocation, { getMatchById });