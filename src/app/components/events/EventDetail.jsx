import React, { useState, useEffect } from "react";
import { getEventById } from "../../actions/Event";
import contentPlaceHolder from "../util/ContentPlaceHolder";
import EventDetailMain from "./EventDetail/EventDetailMain";

const EventDetail = (props) => {
    const [eventModel, setEventModel] = useState(null);

    useEffect(() => {
        getEventModel();
    }, []);

    const getEventModel = async () => {
        const model = await getEventById(props.match.params.eventId);
        setEventModel({ ...model });
    }

    const renderComponent = () => <EventDetailMain {...eventModel} />

    const WrappedLoading = contentPlaceHolder(renderComponent, () => eventModel);

    return <WrappedLoading />
}

export default EventDetail;