import React from 'react';

import notfound from '../../../../../assets/images/image-not-found.jpg'
import { ReducerSubScriptionWithMapStateToProps } from '../../../../others/shares/ReducerSubScription';
import { find } from 'lodash';

class EventBoardImage extends React.Component {
    render() {
        const {
            selectedEvent
        } = this.props;

        const imgUrl = !selectedEvent || !selectedEvent.eventImg || !selectedEvent.eventImg.imgUrl
            ? notfound
            : selectedEvent.eventImg.imgUrl;

        return <img src={imgUrl} className="d-block rounded w-100 animated fadeIn" height="400" />
    }
}

export default ReducerSubScriptionWithMapStateToProps(EventBoardImage, state => {
    return {
        selectedEvent: find(Object.values(state.events), { selectedInBoard: true })
    }
});