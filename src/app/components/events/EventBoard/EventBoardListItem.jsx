import React from "react";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import notfound from '../../../../../assets/images/image-not-found.jpg';
import { onSelectEventBoardModel } from "../../../actions/Event";
import appHistory from "../../../../admin/app.history";

class EventBoardListItem extends React.Component {
    state = {
        animated: false
    }

    componentWillMount() {
        // if (this.props.eventIdList.indexOf(this.props.eventId) === 0)
        //     setTimeout(() => {
        //         this.props.onSelectEventBoardModel(this.props.eventId);
        //     }, 500);
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                animated: true
            })
        }, 800)
    }

    selectEvent = (e) => {
        const {
            eventName,
            eventId
        } = this.props;

        appHistory.push(`/app/event/${eventId}/${eventName}`);
    }

    render() {
        const {
            eventName,
            eventImg,
            eventId,
            selectedList
        } = this.props;

        const cssSelected = selectedList.indexOf(eventId) > -1 ? 'animated bounceIn faster' : '';
        const cssAnimated = !this.state.animated ? 'animated fadeInLeft' : '';

        return (
            <div 
                className={`card col-md-4 col-xs-4 border-0 pointer ${cssSelected} ${cssAnimated}`}
                onClick={this.selectEvent}
            >
                <img src={(eventImg || {}).imgUrl || notfound} className="card-img-top" style={{ height: "220px" }} />
                <div className="card-footer border border-top-0 rounded border-light text-left">
                    <h4 style={{overflow: "hidden", textOverflow: "ellipsis"}}>{eventName}</h4>
                    <div>
                        <Link to={`/app/event/${eventId}/${eventName}`}>
                            รายละเอียด
                        </Link>
                    </div>                    
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    eventIdList: Object.keys(state.events),
    selectedList: Object.values(state.events).filter(data => data.selectedInBoard).map(data => data.eventId)
})

export default connect(mapStateToProps, { onSelectEventBoardModel })(EventBoardListItem);