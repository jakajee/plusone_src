import React from "react";
import _ from 'lodash';
import EventBoardListitem from "./EventBoardListItem";
import { ReducerSubScriptionWithMapStateToProps } from "../../../../others/shares/ReducerSubScription";
import './EventBoardList.scss'

class EventBoardList extends React.Component {

    render() {
        return (
            <div className="scroll-wrapper">
                {this.renderBoardListItem()}
            </div>
        );
    }

    renderBoardListItem = () => {
        return this.props.eventList.map(data => {
            return <EventBoardListitem {...data} key={data.eventId} />
        });
    }

}

export default ReducerSubScriptionWithMapStateToProps(EventBoardList, state => ({
    eventList: Object.values(state.events).slice(0, 6).map(event => ({
        ...event
    }))
}))