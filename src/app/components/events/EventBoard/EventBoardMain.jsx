import React from 'react';
import EventBoardImage from './EventBoardImage';
import EventBoardList from './EventBoardList';

const EventBoardMain = props => {

    return (
        <div className="row">
            <div className="col-12">
                <EventBoardList />
            </div>
        </div>
    )
}

export default EventBoardMain;