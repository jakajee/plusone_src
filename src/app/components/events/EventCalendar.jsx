import React, { Component } from "react";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import _ from 'lodash';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import thLocale from '@fullcalendar/core/locales/th';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';

import { getEventList } from "../../actions/Event";
import moment from 'moment';
import { DATE_TIME_FORMAT } from "../../../others/shares/DateTimeFormat";


class EventCalendar extends Component {    

    render() {
        return (
            <FullCalendar
                defaultView="dayGridMonth"
                plugins={[dayGridPlugin]}
                height={this.props.height || document.body.offsetHeight * 0.7}
                locale={thLocale}
                events={this.props.eventList}
                datesRender={this.onCalendarMonthChange}
                eventClick={this.onClickEvent}
                eventRender={this.onRenderEvent}
            />
        )
    }

    onCalendarMonthChange = ({ view }) => {
        const
            eventDateTimeStart = moment(view.currentStart).format(DATE_TIME_FORMAT),
            eventDateTimeEnd = moment(view.currentEnd).format(DATE_TIME_FORMAT);

        this.props.getEventList(eventDateTimeStart, eventDateTimeEnd);
    }

    onClickEvent = ({ event }) => {
        this.props.history.push(`/app/event/${event.id}/${event.title}`)
    }

    onRenderEvent = (info) => {
    }
}

const mapStateToProps = (state) => {
    return {
        eventList: Object.values(state.events).map(event => ({
            id: event.eventId,
            title: event.eventName,
            start: event.eventDateTimeStart,
            end: event.eventDateTimeEnd,
            className: ['plusone-event', 'pointer']
        }))
    }
}

export default compose(
    withRouter,
    connect(mapStateToProps, { getEventList })
)(EventCalendar);