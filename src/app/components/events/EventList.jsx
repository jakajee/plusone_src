import React, { Component } from "react";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import _ from 'lodash';

import AlertTopic from "../util/AlertTopic";
import EventCalendar from "./EventCalendar";


class EventList extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <AlertTopic alertTitle="กิจกรรม" />
                    <EventCalendar height="100%" />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        eventList: Object.values(state.events)
    }
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(EventList);