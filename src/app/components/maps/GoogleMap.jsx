import React, { PureComponent } from "react";

const markerList = [];

function clearMarker() {
    for (let i = 0; i < markerList.length; i++) {
        markerList[i].setMap(null);
    }

    markerList.length = 0;
}

class GoogleMap extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            gMap: null
        }

        this.renderMarker = this.renderMarker.bind(this);
    }

    componentDidMount() {
        const mapContainer = new google.maps.Map(this.refs['gMap'], {
            zoom: 10,
            // center: {
            //     lat: '',
            //     lng: ''
            // }
        });

        this.setState({
            gMap: mapContainer
        });
        // try {
        //     if (navigator.geolocation) {
        //         navigator.geolocation.getCurrentPosition(position => {
        //             const mapContainer = new google.maps.Map(this.refs['gMap'], {
        //                 zoom: 10,
        //                 center: {
        //                     lat: position.coords.latitude,
        //                     lng: position.coords.longitude
        //                 }
        //             });
    
        //             this.setState({
        //                 gMap: mapContainer
        //             });
        //         })
        //     }
        // } catch (error) {
        //     const mapContainer = new google.maps.Map(this.refs['gMap'], {
        //         zoom: 10,
        //         center: {
        //             lat: '',
        //             lng: ''
        //         }
        //     });

        //     this.setState({
        //         gMap: mapContainer
        //     });
        // }
        
    }

    componentDidUpdate() {
        clearMarker();
        this.renderMarker(this.props.latLngList);
    }

    render() {
        const style = {
            width: "100%",
            height: this.props.height || "100%"
        };

        return (
            <div className="map" style={style} ref="gMap">
            </div>
        );
    }

    renderMarker(latLngList) {
        if (latLngList.length === 0) return;

        const bounds = new google.maps.LatLngBounds();

        latLngList.forEach(latLng => {
            let marker = new google.maps.Marker({
                position: latLng,
                map: this.state.gMap,
                animation: google.maps.Animation.DROP
            });

            let infoWindow = new google.maps.InfoWindow({
                content: `<h4>${latLng.info}</h4>`
            })

            bounds.extend(marker.position);

            marker.addListener('click', () => {
                infoWindow.open(this.state.gMap, marker);
            });

            markerList.push(marker);
        });

        !!this.state.gMap && this.state.gMap.fitBounds(bounds);
    }
}

export default GoogleMap;