import React from 'react';

const G_MAP_API_KEY = "AIzaSyDOx6F-hph_H7EhpomzdF_uSBlEqVhxj8g";

export default function StaticMap(props) {
    const {
        courtLat,
        courtLng,
        height
    } = props;

    return (
        <React.Fragment>
            <img className="img-fluid" src={`https://maps.googleapis.com/maps/api/staticmap?markers=${courtLat},${courtLng}&size=1150x${height}&key=${G_MAP_API_KEY}`} />
        </React.Fragment>
    )
}