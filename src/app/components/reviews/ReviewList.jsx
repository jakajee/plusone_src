import React, { Component } from "react";
import ReviewListItem from "./ReviewListItem";
import { connect } from 'react-redux';
import NotFound from "../util/NotFound";
import { getReviewList } from "../../actions/Review";

class ReviewList extends Component {
    componentDidMount() {
        const { courtId } = this.props;

        this.props.getReviewList(courtId);
    }

    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <h4>ความคิดเห็น</h4>
                    {this.renderReviewListItem()}
                </div>
            </div>
        )
    }

    renderReviewListItem() {
        const { reviewList = [] } = this.props;

        if (reviewList.length === 0) return <NotFound alertMessage="ไม่พบความคิดเห็น" />

        return reviewList.map(item => <ReviewListItem {...item} key={item.uuid} />)
    }
}

function mapStateToProps({ reviewList }) {
    return { reviewList };
}

export default connect(mapStateToProps, { getReviewList })(ReviewList);