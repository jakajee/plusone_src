import React from "react";
import TempImg from "../util/TempImg";
import FaIcon from "../util/FaIcon";
import ReadonlyRating from "../util/ReadonlyRating";

export default (props) => {
    const {
        reviewDescription = "-",
        reviewRating,
        userCreated,
        dateTimeCreated
    } = props

    return (
        <div className="card shadow-sm mt-3">
            <div className="card-header">
                <div className="d-flex flex-row">
                    {/*<TempImg width="40" height="32" className="rounded-circle" />*/}

                    <div className="d-flex align-items-center">
                        <FaIcon faIconName="fa-user fa-3x" />
                    </div>

                    <div className="d-flex flex-column ml-2">
                        <div>
                            <strong className="text-info" style={{ fontSize: "1.4em" }}>{userCreated}</strong>
                            <span className="text-secondary ml-2">{dateTimeCreated}</span>
                        </div>
                        <div className="pb-0">
                            <ReadonlyRating rate={reviewRating} />
                        </div>
                    </div>
                </div>
            </div>

            <div className="card-body">
                <p className="card-text">{reviewDescription}</p>
            </div>


        </div>
    )
}

