import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";

import FaIcon from '../util/FaIcon';
import Modal from '../util/Modal';
import StarRatingComponent from 'react-star-rating-component';
import { createReview, getReviewList } from "../../actions/Review";


class ReviewCreate extends Component {
    constructor(props) {
        super(props);

        this.state = {
            reviewRating: 0,
            reviewDescription: ''
        }

        this.renderModal = this.renderModal.bind(this);
        this.onCreateReview = this.onCreateReview.bind(this);
        this.onCancelReview = this.onCancelReview.bind(this);
    }

    render() {
        return (
            <div className="row mt-3">
                <div className="col">
                    <Modal.ButtonActivate className="btn btn-block btn-lg btn-info" target="#frmCreateReview">
                        <FaIcon faIconName="fa-plus-square" />
                        เพิ่มความคิดเห็น
                    </Modal.ButtonActivate>

                    {this.renderModal()}
                </div>
            </div>
        )
    }

    renderModal() {
        return (
            <Modal
                modalId="frmCreateReview"
                title="เพิ่มความคิดเห็น"
                onSubmitModal={this.onCreateReview}
                onCancelModal={this.onCancelReview}
            >
                <div className="form-horizontal">
                    <div className="form-row">
                        <div className="form-group col-12">
                            <label>ความคิดเห็น</label>
                            <textarea className="form-control" maxLength="255" rows="3" style={{ resize: "none" }}
                                value={this.state.reviewDescription}
                                onChange={(e) => this.setState({ reviewDescription: e.target.value })}
                            ></textarea>
                        </div>
                    </div>

                    <div className="form-row">
                        <div className="col-12 text-right">
                            <StarRatingComponent
                                name="reviewRating"
                                starCount={5}
                                value={this.state.reviewRating}
                                renderStarIcon={() => <FaIcon faIconName="fa-star fa-2x" />}
                                onStarClick={(next) => this.setState({ reviewRating: next })}
                                starColor="#F39C12"
                            />
                        </div>
                    </div>
                </div>
            </Modal>
        )
    }

    onCreateReview() {
        const {
            reviewRating,
            reviewDescription
        } = this.state;

        const courtId = this.props.match.params['courtId'];

        createReview({
            reviewRating,
            reviewDescription,
            masterCourtUuid: courtId,
            userCreated: 'test'
        })
            .payload
            .then(() => this.props.getReviewList(courtId));
    }

    onCancelReview() {

    }
}

const mergedRoute = withRouter(ReviewCreate);

export default connect(null, { getReviewList })(mergedRoute);