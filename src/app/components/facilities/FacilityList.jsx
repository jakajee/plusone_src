import React from "react";
import NotFound from "../util/NotFound";

function renderIcon(facilityList) {
    return facilityList.map((fac, index) => {
        return (
            <span className="badge badge-pill badge-success mx-2" style={{ fontSize: "1em" }} key={index}>
                {fac.facilityName}
            </span>
        );
    })
}

export default (props) => {
    const { tblMasterFacilities = [] } = props;

    const iconList = renderIcon(tblMasterFacilities);

    return (
        <div className="d-flex justify-content-center">
            {iconList.length > 0 ? iconList : <NotFound alertMessage="ไม่พบสิ่งอำนวยความสะดวก" />}
        </div>
    );
}