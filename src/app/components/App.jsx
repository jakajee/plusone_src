import React, { Component } from "react";
import ImageList from "./images/ImageList";
import AlertTopic from "./util/AlertTopic";
import EventCalendar from "./events/EventCalendar";
import { Link } from 'react-router-dom';
import UniversalSearch from "./home/UniversalSearch";
import EventBoardMain from "./events/EventBoard/EventBoardMain";
import QuickLink from "./home/QuickLink";
import QuickGuide from "./home/QuickGuide";
import QuickSearch from "./home/QuickSearch";

export default props => {
    const eventList = [
        {
            eventId: 1,
            eventName: "ช่องMONO29 ถ่ายทอดสดการแข่งขันระหว่าง Mono Vampire vs Singapore Slingers",
            imageUrl: "https://res.cloudinary.com/dngtfua92/image/upload/v1542389133/Activity/45878502_2119643074752410_4623465795165356032_n.jpg"
        },
        {
            eventId: 2,
            eventName: "การแข่งขันบาสเกตบอล THAILAND OPEN 2018 ระหว่างวันเสาร์ที่ 10 พ.ย. 2561 - 13 ก.พ.62",
            imageUrl: "https://res.cloudinary.com/dngtfua92/image/upload/v1542389133/Activity/44517061_1741477389314961_1421949584489316352_n.jpg"
        }
    ]

    const imageList = [
        { imageId: 1, imageUrl: "https://res.cloudinary.com/dngtfua92/image/upload/v1541347641/Avts/a1.jpg", imageActive: "active" },
        { imageId: 2, imageUrl: "https://res.cloudinary.com/dngtfua92/image/upload/v1541347681/Avts/a2.jpg" }
    ];

    return (
        <React.Fragment>

            <QuickSearch />

            <QuickGuide />

        </React.Fragment>
    );
}