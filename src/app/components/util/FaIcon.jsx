import React from 'react';
import PropTypes from "prop-types";

function FaIcon(props) {
    return (
        <React.Fragment>
            <i className={`${props.style || 'fas'} mr-1 ${props.faIconName || props.name}`}></i> {props.label || props.title || ''}
        </React.Fragment>
    );
}

FaIcon.propTypes = {
    faIconName: PropTypes.string
}

export default FaIcon;