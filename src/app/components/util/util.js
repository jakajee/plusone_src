import moment from 'moment';

export function getTimeDatasource(selectedDate = moment().format('YYYY-MM-DD'), excludeDisable = false) {
    let hours = [];

    const getTimeFormat = (hour, minutes) => `${('' + hour).padStart(2, '0')}:${minutes}`;
    const currentDateTime = moment();

    for (let hour = 0; hour <= 24; hour++) {
        let strTimeHour = getTimeFormat(hour, '00');
        let strTimeHalfHour = getTimeFormat(hour, '30');

        let selectedDateTimeHour = moment(`${selectedDate} ${strTimeHour}`);
        let selectedDateTimeHalfHour = moment(`${selectedDate} ${strTimeHalfHour}`);

        hours.push({
            text: strTimeHour,
            value: strTimeHour,
            disabled: excludeDisable === true ? false : selectedDateTimeHour.isBefore(currentDateTime)
        });

        if (hour !== 24)
            hours.push({
                text: strTimeHalfHour,
                value: strTimeHalfHour,
                disabled: excludeDisable === true ? false : selectedDateTimeHalfHour.isBefore(currentDateTime)
            })
    }

    return hours;
}