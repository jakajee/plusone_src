import React, { Component } from 'react';
import Inputmask from 'inputmask/dist/inputmask/inputmask.date.extensions';

export default class TimeInput extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const im = new Inputmask("datetime", {
            inputFormat: "HH:MM",
        });

        im.mask(this.refs["$inputMask"]);
    }

    render() {
        return (
            <React.Fragment>
                <input type="text" {...this.props} ref="$inputMask" placeholder="HH:MM" />
            </React.Fragment>
        )
    }
}
