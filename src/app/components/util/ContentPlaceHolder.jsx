import React, { Component } from 'react';

export const Loader = (props) => {
    return (
        <div className="d-flex justify-content-center">
            <i className="fas fa-spinner fa-3x fa-spin"></i>
        </div>
    )
}

export default function contentPlaceHolder(ComposedComponent, validateFn) {
    class ContentPlaceHolder extends Component {
        render() {
            if (!validateFn()) {
                return (
                    <Loader />
                );
            } else {
                return <ComposedComponent {...this.props} />
            }
        }
    }

    return ContentPlaceHolder;
}