import React, { Component } from 'react';
import { connect } from 'react-redux';

export default function (ChildComponent) {
    class ComposedComponent extends Component {
        componentDidMount() {
            this.shouldNavigateAway();
        }

        componentDidUpdate() {
            this.shouldNavigateAway();
        }

        shouldNavigateAway() {
            if (!this.props.auth) {
                window.location = '/login';
            }
        }

        render() {
            return <ChildComponent {...this.props} />
        }
    }

    function mapStateToProps(state) {
        return { auth: state.auth }
    }

    return connect(mapStateToProps)(ComposedComponent);
}