import React, { Component } from 'react';
import PropTypes from 'prop-types'
import FaIcon from './FaIcon';

class Modal extends Component {

    constructor(props) {
        super(props);

        this.handleCancel = this.handleCancel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }

    componentWillUnmount() {
        $(this.refs['container']).off('hidden.bs.modal');
        $(this.refs['container']).off('shown.bs.modal');
    }

    componentDidMount() {
        if(!!this.props.onShownModal) {
            $(this.refs['container']).on('shown.bs.modal', this.props.onShownModal);
        }
    }

    render() {
        const {
            size = "",
            title = "หัวข้อ Modal",
            submitText = "ยืนยัน",
            closeText = "ยกเลิก",
            modalId = "modal"
        } = this.props;

        return (
            <div ref="container" className={`modal fade`} role="dialog" tabIndex="-1" id={modalId}>
                <div className={`modal-dialog ${size}`}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">{title}</h4>
                            <button type="button" className="close" onClick={this.handleCancel} style={{}}>
                                <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {this.props.children}
                        </div>
                        <div className="modal-footer">
                            <button className="btn btn-secondary" type="button" onClick={this.handleCancel}>{closeText}</button>
                            <button className="btn btn-primary" type="button" onClick={this.handleSubmit}>{submitText}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    handleSubmit() {
        const result = this.props.onSubmitModal && this.props.onSubmitModal();

        if(result !== false)
            this.hideModal();
    }
    handleCancel() {
        this.hideModal();
        this.props.onCancelModal && this.props.onCancelModal();
    }

    hideModal() {
        $(this.refs['container'])
            .modal('hide');
    }


}

Modal.propTypes = {
    size: PropTypes.string,
    title: PropTypes.string,
    submitText: PropTypes.string,
    closeText: PropTypes.string
};

Modal.ButtonActivate = function(props) {
    return (
        <button className={`${props.className}`} type="button" 
            data-toggle="modal"
            data-backdrop="static"
            data-target={`${props.target}`}
            data-keyboard="false"
        >
            {props.children}
        </button>
    )
}

export default Modal;
