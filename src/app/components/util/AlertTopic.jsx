import React from "react";


export default ({ alertStyle = "alert-light", alertTitle = "Title" }) => {
    return (
        <div className={`alert alert-topic ${alertStyle} text-center text-uppercase border`}>
            <h3 className="topic">{alertTitle}</h3>
        </div>
    )
}