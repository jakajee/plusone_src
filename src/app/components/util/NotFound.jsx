import React from "react";

const NotFound = (props) => {
    return (
        <div className="col-12 text-center">
            <strong className="text-danger">{props.alertMessage || 'Not Found !!'}</strong>
        </div>
    );
}

export default NotFound