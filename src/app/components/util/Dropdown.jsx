import React, { Component } from 'react';

export default class Dropdown extends Component {

    constructor(props) {
        super(props);

        this.renderOption = this.renderOption.bind(this);
    }

    render() {
        return (
            <React.Fragment>
                <select {...this.props} >
                    {this.renderOption()}
                </select>
            </React.Fragment>
        )
    }

    renderOption() {
        const { datasource = [] } = this.props;

        return datasource.map((item, index) => {
            return <option {...item} key={index}>{item.text}</option>
        })
    }
}