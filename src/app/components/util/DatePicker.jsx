import React from 'react';
import Flatpickr from 'react-flatpickr';
import "flatpickr/dist/themes/material_blue.css"

export default function DatePicker(props) {
    return (
        <React.Fragment>
            <Flatpickr
                {...props}
            />
            
        </React.Fragment>
    )
}