import React from 'react';
import FaIcon from './FaIcon';
import StarRatingComponent from 'react-star-rating-component';

function ReadonlyRating(props) {
    const { rate = 0, size = "" } = props;

    return (
        <StarRatingComponent
            name={`$star_${Math.ceil(Math.random() * 1000)}`}
            starCount={5}
            value={rate}
            renderStarIcon={() => <FaIcon name={`fa-star ${size}`} />}
            renderStarIconHalf={() => {
                return (
                    <span className="text-warning">
                        <span style={{ position: 'absolute' }}><i className={`far fa-star ${size}`} /></span>
                        <span><i className={`fas fa-star-half ${size}`} /></span>
                    </span>
                );
            }}
            editing={false}
            starColor="#F39C12"
            emptyStarColor="#e0e0e0"
        />
    )
}

export default ReadonlyRating;