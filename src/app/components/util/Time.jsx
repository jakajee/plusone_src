import React from 'react';

export default (props) => {
    const {
        value = ""
    } = props;

    return <span {...props}>{!!value ? value.slice(0, 5) : value}</span>;
}