import React, { Component } from "react";

class ImageList extends Component {

    componentDidMount() {
        $(this.refs.imageList).carousel();
    }

    render() {
        let imageList = this.props.imageList || [];

        const next = imageList.length > 0
            ? (<a className="carousel-control-prev pointer" data-target="#carouselImageList" role="button" data-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="sr-only">Previous</span>
            </a>)
            : null;
        const previous = imageList.length > 0
            ? (<a className="carousel-control-next pointer" data-target="#carouselImageList" role="button" data-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="sr-only">Next</span>
            </a>)
            : null;

        return (
            <div id="carouselImageList" ref="imageList" className="carousel slide">

                <ol className="carousel-indicators">
                    {this.renderIndicatorItem()}
                </ol>

                <div className="carousel-inner">
                    {this.renderImageListItem()}
                </div>

                {next}
                {previous}

            </div>
        )
    }

    renderImageListItem() {
        let imageList = this.props.imageList || [];        

        return imageList.map((item, index) => {
            return (
                <div className={`carousel-item ${index === 0 ? "active" : ""}`} key={item.imageId || index}>
                    <img src={item.imageUrl} className="d-block rounded w-100" />
                    {
                        !!item.imageDescription
                            ? (<div className="carousel-caption d-none d-md-block">
                                <h4>{item.imageDescription}</h4>
                            </div>)
                            : ""
                    }

                </div>
            )
        });
    }

    renderIndicatorItem() {
        let imageList = this.props.imageList || [];        

        return imageList.map((item, index) =>
            <li key={item.imageId || index} data-slide-to={index} className={item.imageActive} data-target="#carouselImageList" />
        );
    }
}

export default ImageList;