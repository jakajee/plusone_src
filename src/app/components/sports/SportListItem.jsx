import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { searchCourt } from '../../actions/Court';

class SportListItem extends Component {
    constructor(props) {
        super(props);

        this.navigate = this.navigate.bind(this);
    }

    render() {
        const { props } = this;

        const icon = !!props.imgUrl
            ? <img className="grow" src={props.imgUrl} style={{width: "48px", height: "auto"}} />
            : <div className="text-danger"><i className={props.cssIcon}></i></div>;

        return (
            <li className="nav-item">
                <a className="nav-link text-decoration-none pointer" onClick={this.navigate}>
                    {icon}
                </a>
            </li>
        )
    }

    navigate() {
        const { history, sportId } = this.props;

        history.push(`/app/court_search/${sportId}`);

        this.props.searchCourt({
            sportId,
            courtName: ''
        })
    }
}

const composed = compose(
    withRouter,
    connect(null, { searchCourt })
)(SportListItem)

export default composed;