import React, { PureComponent } from "react";
import SportListItem from "./SportListItem";
import NotFound from "../util/NotFound";
import AlertTopic from "../util/AlertTopic";
import { getSportTypeList } from "../../actions/Sport";

export default class SportList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            sportTypeList: []
        }
    }

    componentDidMount() {
        getSportTypeList()
            .then(sportTypeList => this.setState({ sportTypeList }))
    }

    render() {
        return (
            <ul className="nav justify-content-md-center justify-content-sm-start container sport flex-nowrap auto-scroll-x">
                {this.renderSportListItem()}
            </ul>
        )
    }

    renderSportListItem() {
        const { sportTypeList } = this.state;

        if (!sportTypeList) return <NotFound />;

        return sportTypeList.map(sport => <SportListItem {...sport} key={sport.uuid} />);
    }
}
