import React, { Component } from "react";
import { getSportTypeList } from "../../actions/Sport";
import { find } from 'lodash';

class SportName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sportTypeList: []
        }
    }

    componentDidMount() {
        getSportTypeList()
            .then(sportTypeList => this.setState({ sportTypeList }));
    }

    render() {
        const { sportId } = this.props;
        const { sportTypeList } = this.state;

        if (sportTypeList.length === 0) return <span>กำลังโหลดข้อมูล...</span>
        else {
            const sportName = find(sportTypeList, { sportId }).sportName;
            return <span>{sportName}</span>
        }

    }
}

export default SportName;
