import React, { Component } from "react";
import MatchItem from "./MatchItem";
import NotFound from '../util/NotFound';
import MatchCreate from "./MatchCreate";
import MatchDetail from "./MatchDetail";
import moment from 'moment';
import DatePicker from "../util/DatePicker";
import { connect } from 'react-redux';
import FaIcon from "../util/FaIcon";
import { getMatchList } from "../../actions/Match";
import { withRouter } from "react-router";
import { compose } from "redux";

class MatchList extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            scheduleDate: props.match.params.matchDate || moment().format("YYYY-MM-DD") 
        }

        this.renderMatchItem = this.renderMatchItem.bind(this);
    }

    componentDidMount() {
        this.props.getMatchList(this.props.uuid, this.state.scheduleDate);
    }

    onScheduleDateChange = (dateObj, scheduleDate) => {
        this.setState({
            scheduleDate
        }, () => {
            this.props.getMatchList(this.props.uuid, scheduleDate);
        })
    }

    render() {
        return (
            <div className="col-12">
                <h4>
                    ตารางแข่งขัน : &nbsp;
                    <small>
                        <DatePicker
                            className="btn btn-danger"
                            value={this.state.scheduleDate}
                            placeholder="YYYY-MM-dd"
                            onChange={this.onScheduleDateChange}
                        />
                    </small>
                </h4>

                {this.renderMatchTable()}

                <MatchCreate scheduleDate={this.state.scheduleDate} />
                <MatchDetail />
            </div>
        )
    }

    renderMatchTable = () => {
        const {
            tblTransactionMatches = []
        } = this.props;

        if (tblTransactionMatches.length === 0) return <NotFound alertMessage="ไม่พบการแข่งขัน" />

        return (
            <div className="table-responsive" style={{ maxHeight: "100%" }}>
                <table className="table table-sm table-hover table-bordered">
                    <thead className="bg-light">
                        <tr>
                            <th>ชื่อการแข่งขัน</th>
                            <th className="text-center">เวลาเริ่ม-สิ้นสุด</th>
                            <th className="text-center">ผู้เล่น/ทั้งหมด</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderMatchItem()}
                    </tbody>
                </table>
            </div>
        )
    }

    renderMatchItem() {
        const {
            tblTransactionMatches = []
        } = this.props;
        return tblTransactionMatches.map(match =>
            <MatchItem
                match={match}
                key={match.uuid}
            />
        );
    }

}

const mapStateToProps = (state, ownProps) => {
    return { tblTransactionMatches: Object.values(state.matchModel.matchList), uuid: ownProps.match.params.courtId }
}

const targetComponent = compose(
    withRouter,
    connect(mapStateToProps, { getMatchList })
)(MatchList)

export default targetComponent;