import React from 'react';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';

export default function MatchUserItem(props) {
    const { tblMasterUser = {} } = props;
    const img = !tblMasterUser || !tblMasterUser.profileImgUrl        
        ? <i className="fas fa-user" style={{fontSize: "5.8rem"}}></i>
        : <img className="rounded-circle img-fluid" src={tblMasterUser.profileImgUrl} style={{height: "80px"}} />
    const displayName = !tblMasterUser ? '-' : (tblMasterUser.displayName || tblMasterUser.username);
    return (
        <div className="col-md-3 text-center">
            {img}
            <OverlayTrigger
                placement="bottom"
                overlay={
                    <Tooltip id={props.masterUserUuid}>
                        {displayName}
                    </Tooltip>
                }
            >
                <div className="text-truncate">{displayName}</div>
            </OverlayTrigger>
        </div>
    )
}