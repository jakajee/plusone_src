import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import { connect } from 'react-redux';
import { hideMatchDetail } from '../../actions/Match'
import MatchUserItem from './MatchUserItem';
import moment from 'moment';

class MatchDetail extends Component {
    constructor(props) {
        super(props);

        this.handleHide = this.handleHide.bind(this);
    }

    render() {
        const { showModal, matchModel } = this.props;
        return (
            <Modal show={showModal} onHide={this.handleHide} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>
                        รายละเอียดการแข่งขัน: &nbsp;
                        <strong className="text-success">{matchModel.matchName}</strong>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    
                <strong className="text-warning"> เริ่ม {moment(matchModel.matchTimeStart, "HH:mm:ss").format("HH:mm")} - {moment(matchModel.matchTimeEnd, "HH:mm:ss").format("HH:mm")}</strong>

                    <div className="card  mt-2">
                        <div className="card-header bg-light">
                            รายชื่อผู้เล่น
                            <span className="ml-1">{(matchModel.tblTransactionMatchUsers || []).length}</span>/<span>{matchModel.matchCapability}</span>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                {this.renderMatchUserItem()}
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button type="button" className="btn btn-secondary" onClick={this.handleHide}>
                        ปิด
                    </button>
                </Modal.Footer>

            </Modal>
        )
    }

    handleHide() {
        this.props.hideMatchDetail();
    }

    renderMatchUserItem() {
        const { matchModel: { tblTransactionMatchUsers = [] } } = this.props;

        if (tblTransactionMatchUsers.length === 0) return;

        return tblTransactionMatchUsers.map((user, index) => {
            return <MatchUserItem {...user} key={index} />
        })
    }
}

function mapStateToProps({ matchModel }) {
    return { matchModel: matchModel.matchDetail, showModal: matchModel.matchDetail.showModal }
}


export default connect(mapStateToProps, { hideMatchDetail })(MatchDetail);