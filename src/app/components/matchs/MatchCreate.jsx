import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { withRouter } from 'react-router'
import _ from 'lodash';
import { toast } from 'react-toastify';

import FaIcon from '../util/FaIcon';
import Modal from '../util/Modal';
import { createMatch } from '../../actions/Match';
import { getCurrentUser, getCurrentUserInfo } from '../../actions/User';
import DatePicker from '../util/DatePicker';
import Dropdown from '../util/Dropdown';
import { getTimeDatasource } from '../util/util';

function getInitialState() {

    const dateNow = moment();
    const dateNowStr = dateNow.format('YYYY-MM-DD');
    const defaultTime = getDefaultTime();

    return {
        showModal: false,
        matchName: "",
        matchCapability: 1,
        matchDate: dateNowStr,
        matchTimeStart: defaultTime,
        matchTimeEnd: defaultTime,
        matchDescription: "",
        matchUserList: []
    }
}

function getDefaultTime() {
    const timeList = getTimeDatasource();
    const index = timeList.findIndex(item => !item.disabled);

    return timeList[index].value;
}

class MatchCreate extends Component {

    constructor(props) {
        super(props);

        this.state = getInitialState();

        this.onCreateMatch = this.onCreateMatch.bind(this);
        this.onShownModal = this.onShownModal.bind(this);
        this.isValidData = this.isValidData.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
        this.onTimeChange = this.onTimeChange.bind(this);
        
        this.updateDateTimeFromTo = this.updateDateTimeFromTo.bind(this);
        this.getSelecteDateTime = this.getSelecteDateTime.bind(this);
    }

    render() {
        const renderInput = (frmGroupCss, labelStr, component) => {
            return (
                <div className={frmGroupCss}>
                    <label>{labelStr}</label>
                    {component}
                </div>
            )
        }

        return (
            <React.Fragment>
                <div className="mt-2">
                    <Modal.ButtonActivate className="btn btn-block btn-success" target="#frmCreateMatch">
                        <FaIcon faIconName="fa-plus-square" />
                        สร้าง
                    </Modal.ButtonActivate>
                </div>

                <Modal
                    modalId="frmCreateMatch"
                    title="สร้างการแข่งขัน"
                    onSubmitModal={this.onCreateMatch}
                    onShownModal={this.onShownModal}
                >
                    <div className="form-horizontal">
                        <div className="form-row">
                            {
                                renderInput("form-group col-xs-12 col-md-8", "ชื่อการแข่งขัน",
                                    <input type="text" className="form-control" value={this.state.matchName} onChange={e => this.setState({ matchName: e.target.value })} />)
                            }

                            {
                                renderInput("form-group col-xs-12 col-md-4", "จำนวนผู้เล่น",
                                    <Dropdown
                                        className="form-control"
                                        datasource={Array.from({ length: 99 }, (v, k) => ({ text: k + 1, value: k + 1 }))}
                                        onChange={e => this.setState({ matchCapability: e.target.value })}
                                        value={this.state.matchCapability}
                                    />
                                )
                            }
                        </div>

                        <div className="form-row">
                            {
                                renderInput("form-group col-xs-12 col-md-4", "วันที่",
                                    <DatePicker
                                        placeholder="YYYY-MM-dd"
                                        className="form-control bg-white"
                                        value={this.state.matchDate}
                                        onChange={this.onDateChange}
                                        options={{
                                            minDate: moment().format("YYYY-MM-DD")
                                        }}
                                    />)
                            }

                            {
                                renderInput("form-group col-xs-12 col-md-4", "เริ่ม",
                                    <Dropdown
                                        className="form-control"
                                        datasource={getTimeDatasource(this.state.matchDate)}
                                        onChange={this.onTimeChange("matchTimeStart")}
                                        value={this.state.matchTimeStart}
                                    />)
                            }

                            {
                                renderInput("form-group col-xs-12 col-md-4", "สิ้นสุด",
                                    <Dropdown
                                        className="form-control"
                                        datasource={getTimeDatasource(this.state.matchDate)}
                                        onChange={this.onTimeChange("matchTimeEnd")}
                                        value={this.state.matchTimeEnd}
                                    />)
                            }
                        </div>

                        <div className="form-row">
                            {
                                renderInput("form-group col-xs-12 col-md-12", "รายละเอียดอื่นๆ",
                                    <textarea className="form-control" rows="4" style={{ resize: "none" }}
                                        value={this.state.matchDescription}
                                        onChange={e => this.setState({ matchDescription: e.target.value })}
                                    ></textarea>)
                            }
                        </div>


                    </div>
                </Modal>
            </React.Fragment>
        )
    }

    onDateChange(date, dateStr) {
        this.updateDateTimeFromTo(dateStr);
    }

    onTimeChange(propName) {
        return (e) => {
            this.setState({
                [propName]: e.target.value
            }, () => this.updateDateTimeFromTo(this.state.matchDate))
        }
    }

    onCreateMatch() {
        let {
            matchName,
            matchCapability,
            matchDate,
            matchTimeStart,
            matchTimeEnd,
            matchDescription,
            matchUserList
        } = this.state;

        if (!this.isValidData()) return false;

        matchUserList = matchUserList.length === 0 ? [{ masterUserUuid: getCurrentUserInfo().uuid }] : matchUserList;
        matchCapability = matchCapability || 1;

        const courtId = this.props.match.params['courtId'];

        this.props.createMatch({
            masterCourtUuid: courtId,
            matchName: matchName || 'N/A',
            matchCapability,
            matchDate,
            matchTimeStart,
            matchTimeEnd,
            matchDescription,
            userCreated: getCurrentUser(),
            matchUserList
        }, this.props.scheduleDate)
    }

    onShownModal(e) {
        const newData = getInitialState();
        newData.matchDate = this.props.scheduleDate || newData.matchDate;
        this.setState(newData);
    }

    isValidData() {
        let {
            matchName = '',
            matchTimeStart,
            matchTimeEnd
        } = this.state;


        if (!matchName.trim()) {
            toast.warn('กรุณาใส่ชื่อการแข่งขัน');
            return false;
        }

        if (matchTimeStart === matchTimeEnd) {
            toast.warn('เวลาเริ่ม-สิ้นสุด ไม่สามารถเป็นเวลาเดียวกันได้, โปรดลองใหม่อีกครั้ง');
            return false;
        }

        return true;
    }

    updateDateTimeFromTo(dateStr) {
        const currentDate = moment();
        const currentSelectedDateFrom = this.getSelecteDateTime(dateStr, "matchTimeStart");
        const currentSelectedDateTo = this.getSelecteDateTime(dateStr, "matchTimeEnd");

        if (
            currentSelectedDateFrom.isBefore(currentDate) ||
            currentSelectedDateTo.isBefore(currentSelectedDateFrom) ||
            currentSelectedDateTo.isBefore(currentDate)
        ) {
            this.setState({
                matchDate: dateStr,
                matchTimeStart: this.state.matchTimeStart,
                matchTimeEnd: this.state.matchTimeStart
            })
        } else {
            this.setState({
                matchDate: dateStr
            });
        }
    }

    getSelecteDateTime(dateStr, propName) {
        return moment(`${dateStr} ${this.state[propName]}`);
    }
}

const mergedRoute = withRouter(MatchCreate);

export default connect(null, { createMatch })(mergedRoute);