import React, { Component } from 'react';
import { connect } from 'react-redux';

import Time from '../util/Time';
import FaIcon from '../util/FaIcon';
import { joinMatch, getMatchById, isUserInMatch } from '../../actions/Match';
import { toast } from 'react-toastify';
import appHistory from '../../../admin/app.history';

class MatchItem extends Component {
    constructor(props) {
        super(props);

        this.onClickJoin = this.onClickJoin.bind(this);
        this.onClickMatchDetail = this.onClickMatchDetail.bind(this);
    }

    render() {
        let { match } = this.props;

        const { tblTransactionMatchUsers = [] } = match;
        const hasEvent = !!match.tblMasterEvent

        return (
            <tr className={`pointer ${hasEvent ? 'bg-warning' : ''}`} onClick={this.onClickMatchDetail}>
                <td>
                    <strong>
                        {match.matchName}
                    </strong>
                </td>
                <td className="text-center">
                    <Time value={match.matchTimeStart} /> - <Time value={match.matchTimeEnd} />
                </td>
                <td className="text-center">
                    <strong>{tblTransactionMatchUsers.length} / {match.matchCapability}</strong>
                </td>
                <td className="text-center">
                    <button className={`btn btn-sm ${hasEvent ? 'btn-danger' : 'btn-info'}`} type="button" onClick={this.onClickJoin}
                        disabled={tblTransactionMatchUsers.length === match.matchCapability}>
                        <FaIcon faIconName="fa-plus-square" />
                        {!hasEvent ? 'เข้าร่วม' : 'เข้าร่วมกิจกรรม'}
                    </button>
                    {this.renderLinkEventDetail()}
                </td>
            </tr>
        )
    }

    renderLinkEventDetail = () => {
        const { match } = this.props;
        if (!!match.tblMasterEvent) {
            return (
                <div>
                    <a href="javascript:void(0)"className="text-white" onClick={this.onClickLinkEvent}>
                        รายละเอียด
                    </a>
                </div>
            )
        }

        return null;
    }

    onClickLinkEvent = (e) => {
        e.stopPropagation();
        const { match } = this.props;
        appHistory.push(`/app/event/${match.tblMasterEvent.eventId}/${match.tblMasterEvent.eventName}`)
    }

    async onClickJoin(e) {
        e.stopPropagation();

        const resultCheckUserInMatch = await isUserInMatch(this.props.match.uuid);
        if (resultCheckUserInMatch) {
            toast.warn('คุณอยู่ในการแข่งขันนี้แล้ว');
            return;
        }

        this.props.joinMatch(this.props.match.uuid);
    }

    onClickMatchDetail() {
        this.props.getMatchById(this.props.match.uuid);
    }
}

export default connect(null, { joinMatch, getMatchById })(MatchItem);