import React from 'react';
import styled from 'styled-components';
import FaIcon from '../util/FaIcon';

const DivSearch = styled.div`
    height: 100vh;
    > div {
        padding-top: 6rem;
    }
`;

const QuickSearch = props => {
    return (
        <DivSearch className="bg-search2">
            <div className="row justify-content-md-center">
                <div className="col-md-5 col-sm-12 col-12 d-flex justify-content-between">
                    <input type="text" className="form-control form-control-lg mr-2"            placeholder="ค้นหา..."/>
                    <button className="btn btn-lg btn-light">
                        <FaIcon name="fa-search" />
                    </button>
                </div>
            </div>
        </DivSearch>
    )
}

export default QuickSearch;