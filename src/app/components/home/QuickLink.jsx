import React from 'react';
import { Link } from 'react-router-dom';

const QuickLink = props => {
    return (
        <div className="row">
            <div className="col-6">
                <div className="card">
                    <img src="https://via.placeholder.com/50x20" className="card-img-top" />
                    <div className="card-body text-center">
                        <p className="card-text">ค้นหาสนาม</p>
                    </div>
                </div>
            </div>

            <div className="col-6">
                <div className="card">
                    <img src="https://via.placeholder.com/50x20" className="card-img-top" />
                    <div className="card-body text-center">
                        <p className="card-text">ค้นหากิจกรรม</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default QuickLink;