import React from 'react';
import styled from 'styled-components';

const COLOR = {
    'text-danger': '#E74C3C',
    'text-warning': '#F39C12',
    'text-info': '#3498DB'
}

const CardHeader = styled.div`
    background: none;
    border: none;

    text-align: center;

    > img {
        width: 100px;
        height: 100px;
        filter: drop-shadow(2px 4px 6px ${props => COLOR[props.color]})
    }
`;

const QuickGuide = props => {
    return (
        <React.Fragment>
            <h3 className="text-center my-3">วิธีนัดเพื่อนผ่าน PlusOne</h3>
            <div className="row">
                {renderGuide({
                    imgUrl: 'https://res.cloudinary.com/dngtfua92/image/upload/v1563183278/ICON%20LOGO/0798ff869c.png',
                    imgAlt: 'สมัคร',
                    text: 'สมัครเป็นส่วนหนึ่งกับ PlusOne ง่ายๆผ่าน facebook หรือ email เพื่อใช้ฟังก์ชันของเรา',
                    cssTitle: 'text-danger'
                })}
                {renderGuide({
                    imgUrl: 'https://res.cloudinary.com/dngtfua92/image/upload/v1563183278/ICON%20LOGO/basketball-floor-png-3.png',
                    imgAlt: 'ค้นหา',
                    text: 'เลือกกีฬาที่คุณกับเพื่อนๆ ต้องการและหาสนามที่โดนใจจากตัวเลือกกว่า 100 แห่ง',
                    cssTitle: 'text-warning'
                })}
                {renderGuide({
                    imgUrl: 'https://res.cloudinary.com/dngtfua92/image/upload/v1563183278/ICON%20LOGO/177848.png',
                    imgAlt: "Let's Fun",
                    text: 'สร้างแมตซ์แข่งขันระหว่างคุณกับแก๊งเพื่อนจากสนามที่คุณได้เลือกแล้วไปสนุกให้สุดเหวี่ยง',
                    cssTitle: 'text-info'
                })}
            </div>
        </React.Fragment >
    )

    function renderGuide({ imgUrl, imgAlt, text, cssTitle }) {
        return (
            <div className="col-md-4 col-sm-4">
                <div className="card border-0">
                    <CardHeader className="card-header" color={cssTitle}>
                        <img className="card-img-top" src={imgUrl} alt={imgAlt} />
                    </CardHeader>
                    <div className="card-body">
                        <h5 className={`card-title text-center ${cssTitle}`}>{imgAlt}</h5>
                        <p className="card-text">{text}</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default QuickGuide;