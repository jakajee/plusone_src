import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import FaIcon from '../util/FaIcon';
import Dropdown from '../util/Dropdown';
import appHistory from '../../../admin/app.history';
import { getSportTypeList } from '../../actions/Sport';
import logoPlusOne from '../../../../assets/images/logoPlusOne.png';


const UniversalSearch = props => {
    const [sportList, setSportList] = useState([]);
    const [sportTypeId, setSportTypeId] = useState('');
    const [keyword, setKeyword] = useState('');

    async function fetchSportList() {
        const response = await getSportTypeList();
        setSportList(response.map(data => ({
            text: data.sportName,
            value: data.sportId
        })));
        setSportTypeId(response[0].sportId);
    }

    useEffect(() => {
        fetchSportList();
    }, []);

    const onSubmit = (e) => {
        e.preventDefault();
        appHistory.push(`/app/court_search/${sportTypeId}?keyword=${keyword}`)
    }

    return (
        <form onSubmit={onSubmit} style={{ height: "370px", marginBottom: "-86px" }}>

            <div className="row align-items-center bg-search mx-0" style={{ top: "-86px" }}>
                <div className="col mx-0">
                    <div className="row justify-content-md-center py-2">
                        <div className="col-md-12 col-sm-12 col-12 text-center">
                            <Link className="logo" to="/app">
                                <img src={logoPlusOne} />
                            </Link>
                        </div>
                    </div>
                    <div className="row justify-content-md-center py-2 text-white">
                        <div className="col-md-12 col-sm-12 col-12 text-center">
                            <h2>ค้นหากิจกรรม แมทซ์แข่งขัน สนามกีฬาใกล้ตัวคุณ</h2>
                        </div>
                    </div>
                    <div className="row justify-content-md-center py-2">
                        <div className="col-md-5 col-sm-12 col-12 d-flex justify-content-between">
                            <Dropdown
                                className="mr-2 col-3 form-control form-control-lg"
                                datasource={sportList}
                                value={sportTypeId}
                                onChange={event => setSportTypeId(event.target.value)}
                            />
                            <input type="text" className="form-control form-control-lg mr-2" placeholder="ค้นหา..."
                                value={keyword}
                                onChange={e => setKeyword(e.target.value)}
                            />
                            <button type="submit" className="btn btn-lg btn-light">
                                <FaIcon name="fa-search" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    )
}



export default UniversalSearch;