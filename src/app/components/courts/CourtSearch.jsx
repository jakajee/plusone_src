import React, { Component } from "react";
import { withRouter } from 'react-router';

import AlertTopic from "../util/AlertTopic";
import GoogleMap from "../maps/GoogleMap";
import SearchBar from "../courts/SearchBar";
import LocationList from "./LocationList";
import CourtList from "./CourtList";
import { connect } from 'react-redux';
import contentPlaceHolder from "../util/ContentPlaceHolder";


class CourtSearch extends Component {

    constructor(props) {
        super(props);

        this.getLatLngList = this.getLatLngList.bind(this);
    }

    render() {

        const { location } = this.props;

        return (
            <React.Fragment>

                <div className="row">
                    <div className="col-md-12">
                        <AlertTopic alertTitle="ค้นหา" alertStyle="alert-danger" />
                    </div>
                </div>

                <div className="row">
                    {/* <div className="col-md-3">
                        <LocationList />
                    </div>
                    */}

                    <div className="col-md-12">
                        <div className="row justify-content-md-center">
                            <div className="col-md-8 mb-3">
                                <SearchBar />
                            </div>
                        </div>

                        <div className="row py-3 border-top border-secondary">
                            <div className="col-md-6">
                                <div className="row">
                                    <CourtList />
                                </div>
                            </div>

                            <div className="col-md-6 mt-3">
                                <GoogleMap latLngList={this.getLatLngList()} />
                            </div>
                        </div>

                        {/*
                        <div className="row">
                            <div className="col-md-12">
                                <GoogleMap />
                            </div>
                        </div>
                        */}
                    </div>
                </div>

            </React.Fragment>
        )
    }

    getLatLngList() {
        return this.props.courtList
            .filter(item => !!item.court_lat && !!item.court_lng)
            .map(item => {
                let coord = new google.maps.LatLng(item.court_lat, item.court_lng);
                coord.info = item.court_name

                return coord;
            });
    }
}

function mapStateToProps({ courtList }) {
    return { courtList }
}

const component = connect(mapStateToProps)(CourtSearch);

export default withRouter(component);