import React from "react";
import { Link, withRouter } from "react-router-dom";
import { Tooltip, OverlayTrigger } from 'react-bootstrap';

import TempImg from "../util/TempImg";
import '../../../../style/components/CourtListItem.scss'

const CourtListItem = (props) => {
    const path = {
        pathname: `/app/court_search/${props.match.params.sportId}/${props.uuid}/${props.court_name}`
    }

    let { court_image_list } = props;
    court_image_list = court_image_list || [];

    let css = "card-img-top img-fluid img-thumbnail img-court-list-item"

    const img = court_image_list.length === 0
        ? <TempImg height={101} alt={props.court_name} className={css} />
        : <img src={court_image_list[0].imageUrl} className={css} />;

    return (
        <div className="col-xs-12 col-sm-6 col-lg-4 col-md-6 mb-3 animated zoomIn">
            <Link
                to={path}
                className="text-decoration-none"
            >
                <div className="card border border-secondary shadow">
                    {img}

                    <div className="card-body">
                        <OverlayTrigger
                            placement="bottom"
                            overlay={
                                <Tooltip id={props.uuid}>
                                    <h6 className="card-text" title={props.court_name}>
                                        {props.court_name}
                                    </h6>
                                </Tooltip>
                            }
                        >
                            <h6 className="card-text text-truncate" title={props.court_name}>
                                {props.court_name}
                            </h6>
                        </OverlayTrigger>

                    </div>
                </div>
            </Link>


        </div>
    );
}

export default withRouter(CourtListItem)