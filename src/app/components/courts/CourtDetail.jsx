import React, { Component } from "react";
import ImageList from "../images/ImageList";
import FacilityList from "../facilities/FacilityList";
import MatchList from "../matchs/MatchList";
import GoogleMap from "../maps/GoogleMap";
import ReviewList from "../reviews/ReviewList";
import AlertTopic from "../util/AlertTopic";
import { connect } from 'react-redux';
import { getCourtById } from "../../actions/Court";
import Time from "../util/Time";
import ReviewCreate from "../reviews/ReviewCreate";
import ReadonlyRating from "../util/ReadonlyRating";
// import contentPlaceHolder from "../util/ContentPlaceHolder";
// import StaticMap from "../maps/StaticMap";

class CourtDetail extends Component {
    constructor(props) {
        super(props);

        this.getLatLngList = this.getLatLngList.bind(this);

    }

    componentWillMount() {
        setTimeout(() => {
            window.scroll({
                top: 0,
                behavior: "smooth"
            })
        }, 100)
    }

    componentDidMount() {
        const courtId = this.props.match.params.courtId;

        this.props.getCourtById(courtId);

    }

    render() {
        const imageList = [
            { imageUrl: "http://via.placeholder.com/800x250", imageActive: "active" },
            { imageUrl: "http://via.placeholder.com/800x250" },
            { imageUrl: "http://via.placeholder.com/800x250" }
        ];

        const { courtModel } = this.props;
        const latLngList = this.getLatLngList(courtModel);

        const renderRowContent = (cssRow, cssCol, content) => {

            return (
                <div className={cssRow}>
                    <div className={cssCol}>
                        {content}
                    </div>
                </div>
            )
        }

        return (
            <React.Fragment>

                <AlertTopic alertStyle="alert-dark" alertTitle={courtModel.courtName} />

                <div className="row animated fill-mode-none fadeIn">
                    <div className="col-md-7 col-lg-6">

                        {renderRowContent("row", "col-12", <ImageList imageList={courtModel.courtImageList || imageList} />)}
                        {renderRowContent("row my-3", "col-12 text-center", <ReadonlyRating rate={courtModel.courtRating} size="fa-3x" />)}
                        {renderRowContent("row mt-3", "col-12", <FacilityList {...courtModel} />)}
                        {renderRowContent("row mt-3", "col-12", this.renderCourtInformation(courtModel))}

                        <ReviewCreate />
                    </div>

                    <div className="col-md-5 col-lg-6">
                        <MatchList {...courtModel} />
                    </div>
                </div>

                <div className="row animated fadeIn my-4">
                    <div className="col-12">
                        {/* <PlaceHolderStaticMap {...courtModel} height={200} /> */}
                        <GoogleMap latLngList={latLngList} height={200} />
                    </div>
                </div>

                <ReviewList courtId={this.props.match.params.courtId} />
            </React.Fragment>
        )
    }

    renderCourtInformation(courtModel) {
        const { tblMasterSportType = {} } = courtModel;
        const cssRow = "form-group row";
        const infoItem = (strongLabel, divCss, spanContent) => {
            return (
                <React.Fragment>
                    <strong className="col-2 col-form-label text-right">
                        {strongLabel}
                    </strong>
                    <div className={divCss}>
                        <span className="form-control-plaintext">
                            {spanContent()}
                        </span>
                    </div>
                </React.Fragment>
            )
        }

        return (
            <div>
                <div className={cssRow}>
                    {infoItem("เวลาเปิด-ปิด", "col-1", () => <React.Fragment><Time value={courtModel.courtOpenHours} /> - <Time value={courtModel.courtCloseHours} /></React.Fragment>)}
                    {infoItem("จำนวนสนาม", "col-1", () => courtModel.courtCount)}
                    {infoItem("ประเภทกีฬา", "col-2", () => tblMasterSportType.sportName)}
                </div>

                <div className={cssRow}>
                    {infoItem("รายละเอียดอื่นๆ", "col-10", () => courtModel.courtDescription || '-')}
                </div>

            </div>
        )
    }

    getLatLngList(courtModel) {
        if (!!courtModel.courtLat && !!courtModel.courtLng) {
            let coord = new google.maps.LatLng({
                lat: courtModel.courtLat,
                lng: courtModel.courtLng
            });
            coord.info = courtModel.courtName;

            return [coord];

        } else {
            return [];
        }
    }
}

function mapStateToProps({ courtModel }) {
    return {
        courtModel
    }
}

const component = connect(mapStateToProps, { getCourtById })(CourtDetail);

export default component;