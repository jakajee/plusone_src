import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { compose } from 'redux';
import { withRouter } from "react-router-dom";
import queryString from 'query-string';

import { searchCourt } from "../../actions/Court";
import SportName from "../sports/SportName";
import FaIcon from "../util/FaIcon";

class SearchBar extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.keyword
        }

        this.onSubmitSearch = this.onSubmitSearch.bind(this);
    }

    componentDidMount() {
        const { match, keyword } = this.props;

        this.props.searchCourt({
            courtName: keyword || this.state.value,
            sportId: match.params['sportId']
        });


    }

    render() {
        const { match } = this.props;

        return (
            <form className="row" onSubmit={this.onSubmitSearch}>
                <div className="col-md-3 form-inline justify-content-md-end justify-content-sm-start">
                    <label htmlFor="txtSearch"><SportName sportId={match.params['sportId']} /></label>
                </div>
                <div className="col-md-8">
                    <div className="input-group">
                        <input type="text" id="txtSearch" className="form-control" placeholder="ค้นหา" autoComplete="off"
                            value={this.state.value}
                            onChange={(event) => this.setState({ value: event.target.value })}
                        />
                        <div className="input-group-append" >
                            <button type="submit" className="btn btn-light">
                                <FaIcon name="fa-search" />
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        )
    }

    onSubmitSearch(event) {
        event.preventDefault();
        const { match } = this.props;
        this.props.searchCourt({
            courtName: this.state.value,
            sportId: match.params['sportId']
        });
    }
}

const mapStateToProps = (state, ownProps) => {
    const { keyword } = queryString.parse(ownProps.location.search);
    return {
        keyword
    }
}

const composedComponent = compose(
    withRouter,
    connect(mapStateToProps, { searchCourt })
)(SearchBar);

export default composedComponent;