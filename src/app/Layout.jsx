import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import NotFound from "./components/util/NotFound";
import Breadcrumb from "./Breadcrumb";
import routesConfig from "./routes.config";
import { map } from 'lodash';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import UserHeader from "./headers/UserHeader";
import MenuHeader from "./headers/MenuHeader/MenuHeader";
import appHistory from "../admin/app.history";

const yearStart = 2018;
const yearNow = new Date().getFullYear();
const config = routesConfig;

function renderRoute() {
    return map(config, (route, index) => {
        return (
            <Route exact {...route} key={index} />
        )
    })
}

function renderBreadCrumb() {
    if(appHistory.location.pathname === '/app')
        return null;

    return (
        <div className="row" id="divBreadCrumb">
            <div className="col">
                <Breadcrumb />
            </div>
        </div>
    )
}

export default (
    <div className="wrapper-all">
        <div className="content">

            <UserHeader />
            <MenuHeader />

            <div className="container my-2 pt-2 pb-4">
                {renderBreadCrumb()}
                <Switch>
                    {renderRoute()}
                    <Route component={NotFound} />
                </Switch>
            </div>
        </div>

        <footer className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="collapse navbar-collapse col-12 d-flex navbar-text justify-content-center">
                <strong>&copy; PLUSONE {yearStart === yearNow ? yearStart : `${yearStart} - ${yearNow}`}</strong>
            </div>
        </footer>
        <ToastContainer autoClose={3000} />
    </div>
)