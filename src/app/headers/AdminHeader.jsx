import React from 'react';
import { getCurrentUserRoleId } from '../actions/User';
import FaIcon from '../components/util/FaIcon';

export default props => {
    if (getCurrentUserRoleId() !== 1) return null;

    return (
        <React.Fragment>
            <a href="/admin" className="px-1 text-decoration-none">
                <FaIcon name="fa-cog"  />
                จัดการข้อมูลระบบ
            </a>
        </React.Fragment>
    )
}