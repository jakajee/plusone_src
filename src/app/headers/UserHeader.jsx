import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { getCurrentUserInfo, logout } from '../actions/User';
import FaIcon from '../components/util/FaIcon';
import AdminHeader from './AdminHeader';
import logoWhite from '../../../assets/images/logoWhite.png';
import styled from 'styled-components';

const NavbarHeader = styled.nav`
    background-color: ${props => props.pathname === '/app' ? "" : "#333333"};
    color: ${props => props.pathname === '/app' ? "" : "#fefefe"};
    z-index: 1;
`

class UserHeader extends Component {
    render() {
        const {
            displayName,
            profileImgUrl,
            username
        } = getCurrentUserInfo();

        const profileImg = !profileImgUrl
            ? <FaIcon faIconName="fa-user" />
            : <img className="rounded-circle mr-1" src={profileImgUrl} width="20" height="20" />
        

        return (
            <React.Fragment>
                <NavbarHeader className="navbar navbar-expand-lg d-flex justify-content-end py-1" pathname={this.props.location.pathname} >
                    <a href="/app" className="px-1 text-decoration-none">
                        <img src={logoWhite} width="12" height="14" className="mr-1" />
                        หน้าหลัก
                    </a>

                    <AdminHeader />

                    <a href="#" className="px-1 text-decoration-none">
                        {profileImg}
                        {displayName || username || 'เข้าสู่ระบบ'}
                    </a>

                    <a href="javascript:void()" className="px-1 text-decoration-none" onClick={logout}>
                        <FaIcon name="fa-power-off" />
                        ออกจากระบบ
                    </a>
                </NavbarHeader>
            </React.Fragment>
        )
    }
}

export default withRouter(UserHeader);