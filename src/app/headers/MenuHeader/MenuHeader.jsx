import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import MenuHeaderItem from './MenuHeaderItem';
import logoPlusOne from '../../../../assets/images/logoPlusOne.png';

const NavbarLink = styled.nav`

    flex-direction: row;
    
    > li {
        align-self: center;
        border-right: 2px solid #d02126;

        @media screen and (max-width: 768px) {
            padding-right: 5px;
            padding-left: 5px;
        }

        :last-child {
            border-right: none;
        }
    }
`;

const NavbarMain = styled.nav`
    display: table;
    width: 100%;

    > * {
        justify-content: center;
        text-align: center;
    }
`;

const DivLogo = styled.div`
    height: 75px;

    .logo {
        top: -25px;
        position: relative;
    }

    img {
        filter: drop-shadow(2px 3px 5px #212121)
    }

    @media screen and (min-width: 768px) {

        .logo > img {
            width: 100px;
            height: 100px;
        }
    }

    @media screen and (max-width: 768px) {

        .logo {
            top: 0;
        }

        .logo > img {
            width: 75px;
            height: 75px;
        }
    }
`;

export default class MenuHeader extends Component {

    render() {
        return (
            <React.Fragment>
                <NavbarMain className="navbar navbar-expand-lg justify-content-center navbar-white py-1"
                >
                    <DivLogo>
                        <Link className="logo" to="/app">
                            <img src={logoPlusOne} />
                        </Link>
                    </DivLogo>
                    <hr style={{ border: "1px solid #212121" }} />
                    <NavbarLink className="navbar-nav">
                        <MenuHeaderItem text="ค้นหาสนาม" />
                        <MenuHeaderItem text="ค้นหากิจกรรม" />
                    </NavbarLink>
                </NavbarMain>
            </React.Fragment>
        )
    }
}