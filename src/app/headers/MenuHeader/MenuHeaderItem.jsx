import React from 'react';
import { withRouter } from 'react-router-dom';

const MenuHeaderItem = props => {
    return (
        <li className="nav-item">
            <a href="#" className="nav-link" style={{fontSize: '1.2rem'}}>
                {props.text}
            </a>
        </li>
    )
}

export default withRouter(MenuHeaderItem);