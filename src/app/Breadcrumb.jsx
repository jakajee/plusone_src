import React from "react";
import { NavLink } from "react-router-dom";
import routesConfig from "./routes.config";
import withBreadcrumbs from "react-router-breadcrumbs-hoc";

const excludePaths = [
    "/app/court_detail",
    "/app/court_search",
    "/app/court_search/:sportId/:courtId",
    "/app/court_search/:sportId/:courtId/:courtName/:matchDate",
    "/app/event/:eventId"
]

const Breadcrumb = ({ breadcrumbs }) => {
    const filteredBreadCrumb = breadcrumbs.filter((b, index) => {
        return excludePaths.indexOf(b.props.match.path) === -1;
    });

    

    return (
        <div className="mb-2">
            {filteredBreadCrumb.map((breadcrumb, index) => (
                <span key={breadcrumb.key}>
                    <NavLink to={breadcrumb.props.match.url}>
                        {breadcrumb}
                    </NavLink>
                    {(index < filteredBreadCrumb.length - 1) && <i> / </i>}
                </span>
            ))}
        </div>
    )
}

export default withBreadcrumbs([routesConfig], {
    excludePaths
})(Breadcrumb);