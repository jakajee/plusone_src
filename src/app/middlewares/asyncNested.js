export default function ({ dispatch, getState }) {    
    return next => action => {
        if (!action || typeof action !== "function")
            return next(action);

        // Make sure the action is function
        action(dispatch, getState);

    }
}