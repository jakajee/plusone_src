import App from "./components/App";
import EventDetail from "./components/events/EventDetail";
import CourtSearch from "./components/courts/CourtSearch";
import CourtDetail from "./components/courts/CourtDetail";
import RequireAuth from "./components/util/RequireAuth";
import SportName from "./components/sports/SportName";
import EventList from "./components/events/EventList";

export default [{
    path: "/app/court_search/:sportId/:courtId",
    component: RequireAuth(CourtDetail),
    breadcrumb: ({ match }) => (<span>รายละเอียดสนาม: {match.params.courtId}</span>),
}, {
    path: "/app/court_search/:sportId/:courtId/:courtName",
    component: RequireAuth(CourtDetail),
    breadcrumb: ({ match }) => (<span>รายละเอียดสนาม: {match.params.courtName}</span>)
}, {
    path: "/app/court_search/:sportId/:courtId/:courtName/:matchDate",
    component: RequireAuth(CourtDetail),
    breadcrumb: ({ match }) => (<span>รายละเอียดสนาม: {match.params.courtName}</span>)
},{
    path: "/app/court_search/:sportId",
    component: RequireAuth(CourtSearch),
    breadcrumb: ({ match }) => <span>ค้นหาสนาม: <SportName sportId={match.params.sportId} /></span>,
}, {
    path: "/app/event/:eventId/:eventName",
    component: RequireAuth(EventDetail),
    breadcrumb: ({ match }) => <span>รายละเอียดกิจกรรม: {match.params.eventName}</span>
}, {
    path: "/app/event/:eventId",
    component: RequireAuth(EventDetail),
    breadcrumb: ({ match }) => <span>รายละเอียดกิจกรรม: {match.params.eventId}</span>
}, {
    path: "/app/event",
    component: RequireAuth(EventList),
    breadcrumb: "กิจกรรม"
}, {
    path: "/app",
    component: RequireAuth(App),
    breadcrumb: "หน้าหลัก"
}, {
    path: "/",
    component: RequireAuth(App),
    breadcrumb: null
}]