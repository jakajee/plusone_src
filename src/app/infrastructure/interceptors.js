import axios from 'axios';
import { getToken } from "../actions/User";

export const setToken = config => {
    const token = getToken();
    config.headers['Authorization'] = `Authorization ${token}`;

    return config;
}

export default function () {
    axios.interceptors.request.use(setToken)
}