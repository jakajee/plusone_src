import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { Router } from "react-router-dom";

import rootReducer from "./reducers/index";
import Layout from "./Layout";

import asyncMd from "./middlewares/async"
import asyncNested from "./middlewares/asyncNested";
import interceptors from "./infrastructure/interceptors";
import { getToken } from "./actions/User";
import appHistory from "../admin/app.history";

interceptors();

const store = createStore(
    rootReducer,
    { auth: getToken() },
    applyMiddleware(asyncMd, asyncNested)
)

const provider =
    <Provider store={store}>
        <Router history={appHistory}>
            {Layout}
        </Router>
    </Provider>

ReactDOM.render(
    provider,
    document.getElementById("body")
);

