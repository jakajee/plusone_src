import { plusoneApp } from '../../api/plusoneApi';

let sportTypeList = [];

export function getSportTypeList() {

    const promise = new Promise((resolve, reject) => {
        if (sportTypeList.length > 0) {
            resolve(sportTypeList);
        } else {
            plusoneApp.get('/sport/list')
                .then(response => {
                    sportTypeList = response.data;
                    resolve(response.data);
                })
                .catch(reject);
        }
    });

    return promise;

}