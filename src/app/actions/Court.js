import axios from "axios";
import moment from "moment";
import { API_PATH_V1 } from "../../config";

export const COURT_SEARCH = "COURT_SEARCH";
export const COURT_GET_BY_ID = "COURT_GET_BY_ID";

export function searchCourt(parameter) {
    const req = axios.get(`${API_PATH_V1}/court/search`, {
        params: parameter
    })

    return {
        type: COURT_SEARCH,
        payload: req,
        params: parameter
    }
}

export function getCourtById(courtId, scheduleDate = null) {
    const dateNow = scheduleDate || moment().format("YYYY-MM-DD");
    const req = axios.get(`${API_PATH_V1}/court/${courtId}?date=${dateNow}`);

    return {
        type: COURT_GET_BY_ID,
        payload: req
    }
}

