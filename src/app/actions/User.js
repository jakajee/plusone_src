import axios from "axios";
import { API_PATH } from "../../config";
import { plusoneRoot } from "../../api/plusoneApi";
import GoogleAuth from "../../others/GoogleAuth";

const APP_TOKEN = 'APP_TOKEN';
const APP_USER = 'APP_USER';
const APP_USER_INFO = 'APP_USER_INFO';

export const GET_USER_INFO = 'GET_USER_INFO';

export const getToken = () => {
    if (!!localStorage.getItem(APP_TOKEN) && !!localStorage.getItem(APP_USER_INFO)) {
        return localStorage.getItem(APP_TOKEN);
    } else {
        return '';
    }
}
export const logout = () => {
    const { systemUserRoleId } = getCurrentUserInfo();

    localStorage.removeItem(APP_TOKEN);
    localStorage.removeItem(APP_USER);
    localStorage.removeItem(APP_USER_INFO);

    if (systemUserRoleId !== 1) {
        socialLogout().then(() => {
            window.location = "/";
        })
        return;
    } else {
        window.location = "/";
    }
}

export const getCurrentUser = () => localStorage.getItem(APP_USER);
export const getCurrentUserInfo = () => JSON.parse(localStorage.getItem(APP_USER_INFO)) || {};
export const getCurrentUserRoleId = () => getCurrentUserInfo().systemUserRoleId;

export function login({ username, password }, callback) {
    const request = axios.post(`${API_PATH}/public/v1/user/login`, {
        username,
        password
    })

    request.then(response => {
        saveUsername(username);
        handleResponse(response);
        setUserInfo()
            .then(() => callback(response));
    })
        .catch(({ response }) => callback(response));
}

export function register({ email, username, password, firstName, lastName }, callback) {
    const request = axios.post(`${API_PATH}/public/v1/user/register`, {
        username,
        email,
        password,
        firstName,
        lastName
    });

    request.then(response => {
        saveUsername(username);
        handleResponse(response);
        setUserInfo()
            .then(() => callback(response));
    })
        .catch(({ response }) => callback(response));
}

export function fbLogin(credential, callback) {
    const {
        email,
        displayName,
        photoURL
    } = credential.user;

    const {
        id,
        first_name,
        last_name
    } = credential.additionalUserInfo.profile;

    createSocialUser({
        username : id,
        email,
        displayName,
        firstName: first_name,
        lastName: last_name,
        photoURL
    }, callback);
}

export const ggLogin = (googleUser, callback) => {
    if (!googleUser.getBasicProfile()) return;

    const basicProfile = googleUser.getBasicProfile(),
        username = basicProfile.getId(),
        email = basicProfile.getEmail(),
        displayName = basicProfile.getName(),
        photoURL = basicProfile.getImageUrl(),
        firstName = basicProfile.getGivenName(),
        lastName = basicProfile.getFamilyName();

    createSocialUser({
        username,
        email,
        displayName,
        photoURL,
        firstName,
        lastName
    }, callback)
}

export function setUserInfo() {
    const resolver = resolve => {
        const username = getCurrentUser();

        const request = getUserInfo(username).payload

        request.then((response) => {
            localStorage.setItem(APP_USER_INFO, JSON.stringify(response.data));
            resolve();
        });
    }

    const promise = new Promise(resolver);
    return promise;
}

export function getUserInfo(username) {

    const request = axios.get(`${API_PATH}/v1/user/${username}`, {
        headers: {
            'Authorization': `Authorization ${localStorage.getItem(APP_TOKEN)}`
        }
    })

    return {
        type: GET_USER_INFO,
        payload: request
    }

}

export function socialLogout() {
    const promise = new Promise(resolve => {
        const googleAuth = new GoogleAuth();

        googleAuth.getCurrentAuth().then(auth => {
            if(auth.isSignedIn.get() === true) 
                auth.signOut();
            resolve();
        });
    });

    return promise;
}

function createSocialUser({ username, email, displayName, firstName, lastName, photoURL }, callback) {
    plusoneRoot.post('/public/v1/user/sociallogin', {
        username: username || email,
        password: "",
        email,
        displayName,
        firstName,
        lastName,
        profileImgUrl: photoURL,
        flagOauth: true
    })
        .then(response => {
            saveUsername(username || email);
            handleResponse(response);

            setUserInfo()
                .then(() => callback(response));
        })
        .catch(({ response }) => callback(response));
}

function saveToken(token) {
    localStorage.setItem(APP_TOKEN, token);
}

function handleResponse(response) {
    if (response.data.data) {
        saveToken(response.data.data.token);
    }
}

function saveUsername(username) {
    localStorage.setItem(APP_USER, username);
}