import axios from 'axios';
import moment from 'moment';
import { filter } from 'lodash';
import { API_PATH_V1 } from '../../config';
import { getCurrentUserInfo } from './User';
import { plusoneApp } from '../../api/plusoneApi';

export const MATCH_CREATE = 'MATCH_CREATE';
export const MATCH_GET_BY_ID = 'MATCH_GET_BY_ID';
export const MATCH_DETAIL_HIDE = 'MATCH_DETAIL_HIDE';
export const MATCH_JOIN = 'MATCH_JOIN';
export const MATCH_LIST = 'MATCH_LIST';

export function createMatch(matchModel, scheduleDate = moment().format('YYYY-MM-DD')) {

    return async dispatch => {
        const response = await axios.post(`${API_PATH_V1}/match/create`, matchModel);
        const responseMatchList = await getMatchList(matchModel.masterCourtUuid, scheduleDate).payload;

        dispatch({
            type: MATCH_CREATE,
            payload: responseMatchList
        })
    }
}

export function getMatchById(matchId) {
    const req = axios.get(`${API_PATH_V1}/match/${matchId}`);

    return {
        type: MATCH_GET_BY_ID,
        payload: req
    }
}

export function hideMatchDetail() {
    return {
        type: MATCH_DETAIL_HIDE
    }
}

export function joinMatch(matchId) {
    return async dispatch => {
        const params = {
            matchUuid: matchId,
            masterUserUuid: getCurrentUserInfo().uuid
        };
        const res = await axios.patch(`${API_PATH_V1}/match/join`, params);
        const responseNewMatchDetail = await getMatchById(matchId).payload;

        dispatch({
            type: MATCH_JOIN,
            payload: responseNewMatchDetail,
            matchUuid: matchId
        })
    }
}

export async function isUserInMatch(matchId) {
    const matchDetail = await getMatchById(matchId).payload;
    const { tblTransactionMatchUsers = [] } = matchDetail.data;
    const user = filter(tblTransactionMatchUsers, { masterUserUuid: getCurrentUserInfo().uuid });
    return user.length > 0;
}

export async function isMatchFull(matchId) {
    const { data } = await getMatchById(matchId).payload;
    const { tblTransactionMatchUsers = [] } = data;

    return tblTransactionMatchUsers.length === data.matchCapability;
}

export function getMatchList(courtId, date) {
    const req = plusoneApp.get(`/match/list/${courtId}/${date}`);

    return {
        type: MATCH_LIST,
        payload: req
    }
}