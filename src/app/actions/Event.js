import { plusoneApp } from "../../api/plusoneApi";

export const GET_EVENT_LIST = 'GET_EVENT_LIST';
export const SELECT_EVENT_BOARD = 'SELECT_EVENT_BOARD';

export const getEventList = (eventDateTimeStart, eventDateTimeEnd) => async dispatch => {
    const response = await plusoneApp.get(`/event?eventDateTimeStart=${eventDateTimeStart}&eventDateTimeEnd=${eventDateTimeEnd}`);
    dispatch({
        type: GET_EVENT_LIST,
        payload: response.data
    })
}

export const getEventById = async eventId => {
    const response = await plusoneApp.get(`/event/${eventId}`);
    return response.data;
}

export const onSelectEventBoardModel = eventId => {
    return {
        type: SELECT_EVENT_BOARD,
        payload: eventId
    }
}