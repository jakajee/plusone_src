import axios from "axios";
import { API_PATH_V1 } from "../../config";

export const REVIEW_CREATE = 'REVIEW_CREATE';
export const REVIEW_UPDATE = 'REVIEW_UPDATE';
export const REVIEW_FETCH = 'REVIEW_FETCH';

export function getReviewList(courtId) {
    const req = axios.get(`${API_PATH_V1}/review/${courtId}`);

    return {
        type: REVIEW_FETCH,
        payload: req
    }
}

export function createReview(reviewModel) {
    const req = axios.post(`${API_PATH_V1}/review/create`, reviewModel);

    return {
        type: REVIEW_CREATE,
        payload: req,
        reviewModel
    }
}

export function updateReview(reviewModel) {
    const req = axios.patch(`${API_PATH_V1}/review/update`, reviewModel);

    return {
        type: REVIEW_UPDATE,
        payload: req,
        reviewModel
    }
}