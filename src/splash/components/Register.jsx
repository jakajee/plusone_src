import React, { Component } from "react";
import { register } from '../../app/actions/User';
import { ToastContainer, toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            confirmPassword: ""
        }

        this.onRegister = this.onRegister.bind(this);
    }

    render() {
        return (
            <form className="form-horizontal font-caviardreams col-md-3" onSubmit={this.onRegister}>
                <div className="form-group">
                    <h4 className="text-white">Email</h4>
                    <input className="form-control" type="text"
                        value={this.state.email}
                        name="username"
                        maxLength="50"
                        onChange={event => this.setState({ email: event.target.value })}
                    />
                </div>

                <div className="form-group">
                    <h4 className="text-white">Password</h4>
                    <input className="form-control" type="password" value={this.state.password} name="password"
                        maxLength="50"
                        onChange={event => this.setState({ password: event.target.value })} />
                </div>

                <div className="form-group">
                    <h4 className="text-white">Confirm Password</h4>
                    <input className="form-control" type="password" value={this.state.confirmPassword} name="confirmPassword"
                        maxLength="50"
                        onChange={event => this.setState({ confirmPassword: event.target.value })} />
                </div>

                <div className="form-group mt-4">
                    <button type="submit" className="btn btn-danger btn-block">Submit</button>
                </div>

                <div className="form-group">
                    <Link to="/login" className="btn btn-block">
                        Back to Login                       
                    </Link>
                </div>
                <ToastContainer autoClose={3000} />
            </form>
        )
    }

    onRegister(e) {
        e.preventDefault();

        const {
            email,
            password,
            confirmPassword
        } = this.state;

        if (
            !this.validateValue(email, 'email') ||
            !this.validateValue(password, 'password') ||
            !this.validateValue(confirmPassword, 'confirm password')
        ) return;


        if (password !== confirmPassword) {
            toast.warn('Please check password and confirm password')
            return;
        }

        register({ username: email, email, password }, ({ data }) => {
            if (data.flagSuccess) {
                window.location = "/app";
            } else {
                toast.error(data.message || 'Register failed, please try again');
            }
        })
    }

    validateValue(value, field) {
        if (!value) {
            toast.warn(`Please input ${field}`);
            return false;
        }

        return true;
    }
}

export default Register;