import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { login, fbLogin, setUserInfo } from '../../app/actions/User';
import { getToken } from '../../app/actions/User';

import { provider, auth } from '../authen/FbAuthen';
import GGAuthen from '../authen/GGAuthen';

const authFirebase = auth();
authFirebase.getRedirectResult()
    .then(credential => {
        if (credential.credential) {
            fbLogin(credential, handlerLoginOrRegister)
        }
    })

function handlerLoginOrRegister(res) {
    if (res.data.flagSuccess) {
        redirectToApp();
    } else {
        toast.error(res.data.message);
    }
}

function redirectToApp() {
    toast.success("Loging in ...");
    setTimeout(() => {
        window.location = "/app";
    }, 500);
}

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: ""
        }

        this.submitLogin = this.submitLogin.bind(this);
        this.loginFacebook = this.loginFacebook.bind(this);
    }

    componentWillMount() {
        if (!!getToken()) {
            // refresh user data
            setUserInfo()
                .then(() => {
                    redirectToApp();
                })
        }
    }

    render() {
        return (
            <form className="form-horizontal font-caviardreams col-md-4 col-lg-3 col-xs-12" onSubmit={event => { event.preventDefault(); this.submitLogin() }}>
                <div className="form-group">
                    <h4 className="text-white">Email</h4>
                    <input className="form-control" type="text" value={this.state.username} name="username" onChange={event => this.setState({ username: event.target.value })} />

                </div>

                <div className="form-group">
                    <h4 className="text-white">Password</h4>
                    <input className="form-control" type="password" value={this.state.password} name="password" onChange={event => this.setState({ password: event.target.value })} />
                </div>

                <div className="form-group d-flex justify-content-between">
                    <button className="btn col-5 btn-danger" type="submit">Login</button>
                    <Link to="/register" className="btn col-5 btn-secondary">
                        Register
                    </Link>
                </div>

                <div className="form-group d-flex text-center">
                    <span className="col-xs-3 col-md-2 line-center">
                        <span></span>
                    </span>
                    <span className="col-md-8 col-xs-6 text-light">Or Connect With</span>
                    <span className="col-xs-3 col-md-2 line-center">
                        <span></span>
                    </span>
                </div>

                <div className="form-group">
                    <button type="button" onClick={this.loginFacebook} className="btn btn-block btn-facebook" style={{ fontSize: "1.23em" }}>
                        <i className="fab fa-facebook-square"></i> Facebook
                    </button>
                </div>

                <div className="form-group">
                    <GGAuthen onLoginSuccess={redirectToApp} />
                </div>

                <ToastContainer autoClose={3000} />
            </form>
        )
    }

    submitLogin() {
        const { username, password } = this.state;
        login({ username, password }, handlerLoginOrRegister);
    }

    loginFacebook() {
        auth().signInWithRedirect(provider);
    }
}