import React, { Component } from "react";
import { Link } from "react-router-dom";
import '../../../style/landing.scss'

export default class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      background_x_position: 0,
      delay: false,
      index: 0
    };
  }

  _bgChangePosX = Xdirection => {
    if (Xdirection === "right") {
      if (this.state.index < 3) {
        this.setState({
          background_x_position: this.state.background_x_position + -500,
          delay: true,
          index: this.state.index + 1
        });
      }
    }

    if (Xdirection === "left") {
      if (this.state.index <= 3 && this.state.index != 0) {
        this.setState({
          background_x_position: this.state.background_x_position + 500,
          delay: true,
          index: this.state.index - 1
        });
      }
    }

    setTimeout(() => {
      this.setState({
        delay: false
      });
    }, 1500);
  };

  render() {
    return (
      <React.Fragment>
        
          <div className={"content"}>
            PLUSONE X {this.state.index === 0 ? "Mohamed Salah" : null}
            {this.state.index === 1 ? "Cristiano Ronaldo" : null}
            {this.state.index === 2 ? "Eden Hazard" : null}
          </div>

          {this.state.index === 0 ? (
            <img
              src={require("../../../assets/content_image1.png")}
              className={"first-img-content right-fade-in"}
            />
          ) : (
            <img
              src={require("../../../assets/content_image1.png")}
              className={"first-img-content right-fade-out"}
            />
          )}
          {this.state.index === 1 ? (
            <img
              src={require("../../../assets/content_image2.png")}
              className={"second-img-content right-fade-in"}
            />
          ) : (
            <img
              src={require("../../../assets/content_image2.png")}
              className={"second-img-content right-fade-out"}
            />
          )}

          {this.state.index === 2 ? (
            <img
              src={require("../../../assets/content_image3.png")}
              className={"third-img-content right-fade-in"}
            />
          ) : (
            <img
              src={require("../../../assets/content_image3.png")}
              className={"third-img-content right-fade-out"}
            />
          )}

          {this.state.delay ? null : (
            <div
              className={"container-button left"}
              onClick={() => {
                this._bgChangePosX("left");
              }}
            >
              left
            </div>
          )}
          {this.state.delay ? null : (
            <div
              className={"container-button right"}
              onClick={() => {
                this._bgChangePosX("right");
              }}
            >
              right
            </div>
          )}
          <div className={"background-dark-blur"} />
          <div
            className={"background"}
            style={{ backgroundPositionX: this.state.background_x_position }}
          />
        
      </React.Fragment>
    );
  }
}
