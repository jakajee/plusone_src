import React from 'react';
import { ggLogin } from '../../app/actions/User';
import GoogleAuth from '../../others/GoogleAuth';

class GGAuthen extends React.Component {
    componentDidMount() {
        const googleAuth = new GoogleAuth();

        googleAuth.getCurrentAuth()
            .then(auth => {
                this.auth = auth;
                this.onAuthChange();
                this.auth.isSignedIn.listen(this.onAuthChange)
            })
    }

    onAuthChange = () => {
        ggLogin(this.auth.currentUser.get(), () => {
            this.props.onLoginSuccess();
        });
    }

    onSignIn = () => {
        this.auth.signIn();
    }

    render() {
        return (
            <button type="button" onClick={this.loginGoogle} className="btn btn-block btn-danger" style={{ fontSize: "1.23em" }} onClick={this.onSignIn}>
                <i className="fab fa-google"></i> Google
            </button>
        )
    }
}

export default GGAuthen;