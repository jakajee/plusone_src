import firebase from 'firebase/app';
import 'firebase/auth';
import { secret } from '../secret'

firebase.initializeApp(secret);

export const auth = firebase.auth;
export const provider = new firebase.auth.FacebookAuthProvider();