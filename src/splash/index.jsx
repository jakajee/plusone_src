import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import '../../style/index.css';
import '../../style/landing.scss'

import Login from './components/Login';
import Landing from './components/Landing';
import Register from './components/Register';


class Index extends Component {

    constructor(props) {
        super(props);

    }

    render() {

        return (

            <div className="landing-container">     
                <BrowserRouter>
                    <React.Fragment>
                        <Route exact path="/" component={Landing} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/register" component={Register} />
                    </React.Fragment>
                </BrowserRouter>
            </div>


        );
    }

}

ReactDOM.render(
    <Index />,
    document.getElementById("root")
);
