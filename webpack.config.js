const
    webpack = require("webpack"),
    HtmlWebpackPlugin = require("html-webpack-plugin"),
    CopyWebpackPlugin = require("copy-webpack-plugin"),
    CleanWebpackPlugin = require("clean-webpack-plugin");


module.exports = (env, argv) => {

    const pathsToClean = [
        "dist"
    ]

    const outputPath = __dirname + "/dist";

    const targetPath = {
        "production": {
            reactCdnPath: "production.min",
            outputPath: outputPath
        },
        "development": {
            reactCdnPath: "development",
            outputPath: outputPath
        }
    }

    const configPath = targetPath[argv.mode];

    const config = {
        entry: {
            // specify your moudle name and the entry point of it
            app: ["@babel/polyfill", "./src/app/app.jsx"],
            admin: ["@babel/polyfill", "./src/admin/app.jsx"],
            splash: ["@babel/polyfill", "./src/splash/index.jsx"]
        },
        output: {
            // [name] is the name of your provided entry[name]
            filename: "[name].js?[hash]",
            path: configPath.outputPath,
            publicPath: "/"
        },
        resolve: {
            extensions: [".js", ".jsx", ".json"]
        },

        module: {
            rules: [{
                test: /\.(js|jsx)?$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env']
                    }
                },
            }, {
                test: /\.(s*)css$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            }, {
                test: /\.(png|jp(e*)g|svg)$/,
                use: [{
                    loader: "url-loader",
                    options: {
                        limit: 8000,
                        name: "assets/images/[name].[ext]"
                    }
                }]
            }, {
                test: /\.(woff(2)?|ttf|eot|svg|otf)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: "file-loader",
                    options: {
                        name: "assets/fonts/[name].[ext]"
                    }
                }]
            }, {
                // All output ".js" files will have any sourcemaps re-processed by "source-map-loader".
                enforce: "pre", test: /\.js$/,
                loader: "source-map-loader"
            }]
        },

        plugins: [
            new CleanWebpackPlugin(pathsToClean, {}),

            new CopyWebpackPlugin([
                { from: "style", to: "style" }
            ]),

            // splash page
            new HtmlWebpackPlugin({
                fileName: "index.html",
                template: "index.html",
                reactMode: configPath.reactCdnPath,
                chunks: ["splash"]
            }),

            // app page
            new HtmlWebpackPlugin({
                filename: "app.html",
                template: "app.html",
                reactMode: configPath.reactCdnPath,
                gMapKey: argv.mode === "production" ? "?key=AIzaSyDOx6F-hph_H7EhpomzdF_uSBlEqVhxj8g" : "",
                chunks: ["app"]
            }),

            // admin page
            new HtmlWebpackPlugin({
                filename: "admin.html",
                template: "admin.html",
                reactMode: configPath.reactCdnPath,
                gMapKey: argv.mode === "production" ? "?key=AIzaSyDOx6F-hph_H7EhpomzdF_uSBlEqVhxj8g" : "",
                chunks: ["admin"]
            }),

            // API Path
            new webpack.EnvironmentPlugin({
                NODE_ENV: "development"
            })
        ],

        // When importing a module whose path matches one of the following, just
        // assume a corresponing global variable exists and use that instead.
        // This is important because it allows us to avoid bundling all of our
        // dependencies, which allows browsers to cache those libraries between builds.
        externals: {
            "react": "React",
            "react-dom": "ReactDOM",
            "$el": "jQuery",
            "google": "google",
            "firebase/app": "firebase",
            "firebase/auth": "firebase"
        },

        devServer: {
            historyApiFallback: {
                rewrites: [
                    { from: /\/app/, to: "/app.html" },
                    { from: /\/admin/, to: "/admin.html" }
                ]
            },
            contentBase: './',
            openPage: "app"
        }
    }



    if (argv.mode === "development") {
        config.plugins.push(new webpack.HotModuleReplacementPlugin());
        config.devServer.hot = true;
        config.devtool = "source-map";
    }

    return config;

}